
def parse_rule(rule):
    tmp = rule.split(";")
    rule_name = tmp[0].strip()
    parameters = tmp[1:]

    return (rule_name, parameters)


def build_rule_declaration(name, rule : str):
    package_name = name + "s"
    rule_name, parameters = parse_rule(rule)

    declaration = f"""   type {rule_name} is new {name} with record"""

    for param in parameters:
        declaration += "\n" + param + ";"

    declaration += f"""\n   end record;
    
   overriding procedure Acceptor
     (Self        : in out {rule_name};
      The_Visitor :        access {package_name}_Visitor.Visitor'Class);"""
    
    return declaration


def build_acceptor_body(name, rule):
    package_name = name + "s"
    rule_name, parameters = parse_rule(rule)

    return f"""   overriding procedure Acceptor
     (Self        : in out {rule_name};
      The_Visitor :        access {package_name}_Visitor.Visitor'Class)
   is
   begin
      The_Visitor.Visit_{rule_name} (Self);
   end Acceptor;"""


def generate_ast(name, incl, rules):
    package_name = name + "s"

    ads = f"""{incl}
with Ada.Containers.Vectors;

limited with {package_name}_Visitor;
package {package_name} is
    type {name} is abstract tagged null record;

   procedure Acceptor
     (Self        : in out {name};
      The_Visitor :        access {package_name}_Visitor.Visitor'Class);

   type {name}_Acc is access {name}'Class;

   package {name}_Vectors is new Ada.Containers.Vectors
     (Index_Type   => Positive,
      Element_Type => {name}_Acc);\n\n"""

    for rule in rules:
        ads += build_rule_declaration(name, rule)
        ads += "\n\n"

    ads += f"""
private
end {package_name};
"""


    adb = f"""with {package_name}_Visitor;

package body {package_name} is

   procedure Acceptor
     (Self        : in out {name};
      The_Visitor :        access {package_name}_Visitor.Visitor'Class)
   is
   begin
      The_Visitor.Visit_{name} (Self);
   end Acceptor;\n\n"""

    for rule in rules:
        adb += build_acceptor_body(name, rule)
        adb += "\n\n"

    adb += f"""
end {package_name};
"""

    visitor_ads = f"""\
with {package_name}; use {package_name};

package {package_name}_Visitor is

   type Visitor is interface;

   procedure Visit_{name}
     (Self : in out Visitor;
      Obj  : in out {package_name}.{name}'Class) is null;
"""

    for rule in rules:
        rule_name, _ = parse_rule(rule)
        visitor_ads += f"""   procedure Visit_{rule_name}
     (Self : in out Visitor;
      Obj  : in out {rule_name}'Class) is abstract;\n"""

    visitor_ads += f"end {package_name}_Visitor;\n"

    with open("src/" + package_name.lower() + ".ads", "w") as f:
        f.write(ads)

    with open("src/" + package_name.lower() + ".adb", "w") as f:
        f.write(adb)

    with open("src/" + package_name.lower() + "_visitor.ads", "w") as f:
        f.write(visitor_ads)

if __name__ == "__main__":
    generate_ast("Expr", """with Tokens;
with Values; use Values;""", [
        "Assign ; Name : Tokens.Token; Value : Expr_Acc; Depth : Natural := 0",
        "Binary ; Left : Expr_Acc; Operator : Tokens.Token; Right : Expr_Acc",
        "Call ; Callee : Expr_Acc; paren : Tokens.Token; Arguments : Expr_Vectors.Vector",
        "Get ; Object : Expr_Acc; Name : Tokens.Token",
        "Grouping ; Expression : Expr_Acc",
        "Literal ; Value : Values.Value",
        "Logical ; Left : Expr_Acc; Operator : Tokens.Token; Right : Expr_Acc",
        "Set ; Object : Expr_Acc; Name : Tokens.Token; Value : Expr_Acc",
        "Super ; Keyword : Tokens.Token; Method : Tokens.Token; Depth : Natural := 0",
        "This ; Keyword : Tokens.Token",
        "Unary ; Operator : Tokens.Token; Right : Expr_Acc",
        "Variable ; Name : Tokens.Token; Depth : Natural := 0",
    ])

    generate_ast("Stmt", """with Tokens;
with Exprs;""", [
        "Block ; Statements : Stmt_Vectors.Vector",
        "Class ; Name : Tokens.Token; Superclass : Exprs.Expr_Acc; Methods : Stmt_Vectors.Vector",
        "Expression ; Ex : Exprs.Expr_Acc",
        "Function_S ; Name : Tokens.Token; Parameters : Tokens.Token_Vectors.Vector; Statements : Stmt_Vectors.Vector",
        "If_S ; Condition : Exprs.Expr_Acc; Then_Branch : Stmt_Acc; Else_Branch : Stmt_Acc",
        "Print ; Ex : Exprs.Expr_Acc",
        "Var ; Name : Tokens.Token; Initializer : Exprs.Expr_Acc",
        "Return_S ; keyword : Tokens.Token; Value : Exprs.Expr_Acc",
        "While_S ; Condition : Exprs.Expr_Acc; Loop_Body : Stmt_Acc",
    ])