// Quotient and modulus

fun quotient(a, b) {
  var i = 0;
  while (a >= b * i) {
    i = i + 1;
  }
  return i - 1;
}

fun modulus(a, b) {
  return a - b * quotient(a, b);
}

print modulus(9, 5);