Diary
=====

Marker: https://craftinginterpreters.com/a-bytecode-virtual-machine.html

2022-12-18
----------

Cleaned up error handling a tiny bit.

2022-12-11
----------

Implmented all of the inheritance chapter. The initial implementation had a bug related to how static resolution behaved for `this` and methods. Since I had deviated from the book, my resolution method did not account for an additional scope. I rewrote the method look-up code to look through this additional scope. Apart from that, there wasn't a whole lost interesting in it.

This also concludes the implementaton of the tree-walking interpreter in Ada. It has been a real journey and a great to learn about compilers/interpreters and Ada!

2022-11-13
----------

Finished the last pieces of the class implementation with some semantics around initializers and return statements. Nothing especially interesting in Ada-terms.

2022-11-09
----------

Static analysis of `this` use and class initializers. Not too eventful but just some nice straight-forward code.

2022-11-06
----------

As mentioned in the previous entry, my implementation deviated from the original implementation. This deviation was a problem for how the `this` keyword is handled; or more specifically, for how methods are bound to an instance. My implementation did not distinguish between methods and other properties and instead kept both within the same environment within the instance.

I split this into two environments, one for general properties and one for methods. If a field associated with a method is accessed, then the binding of the method to the instance occurs. This was simple enough in the end.

2022-10-21
----------

Implemented methods. I decided to deviate a bit from the book and instead continue to use the facilities of the Environment type instead of the more "raw" approach in the book. However, now that I am reading a bit ahead, I do see that there might be a good reason for the other approach.

To correctly handle a bunch of edge cases around method binding it is necessary to handle some very different scenarios from the Environment type. And when the scenarios are different enough, why not just directly handle those use cases.

2022-10-17
----------

Yesterday it was getters -- today, setters. Not much interesting w.r.t Ada.

2022-10-16
----------

Implemented property getters -- setters are coming in the next session. I've decided to let the properties reside in Environment types rather than creating a new hashed map.

2022-10-15
----------

Implemented the type for instances and fitted it into the dynamic value type.

2022-10-12
----------

Started work on classes. Ultimately, it is nice that a lot of the implementation for functions can be reused. This was just a quick work-session, but it is always good to get started.

2022-10-02
----------

Finished the resolver. Satisfied with how the solution turned out. While the jlox implementation fits well with the book, I don't think my approach is too complicated. I do however wonder about the efficiency of the look-up method since there are several functon calls.

2022-10-01
----------

This marks the first major deviation between Ada_Lox and jlox. I could not figure out a good way to have the expressions in a hash map. Instead I put the necessary information from the resolver into the expression tree.

2022-09-29
----------

I expected this to mostly be a bunch of busy work, but it turned to somewhat surprising bug hunt. In the end, I realized that when I have a vector of maps, then the maps are somehow (perhaps) when I select them from the vector. They are at least not references, which are mutated "in" vector when new elements are added. I have find a nicer way of doing this.

It would also be nice to find a way to use lexeme strings directly in hashed maps.

2022-09-27
----------

Maps and vectors of maps. Busy work for getting ready to implement the logic of the resolver.

2022-09-26
----------

Finished up the chapter on functions and implemented the simple (but wrong) closure implementation. The return to this project has been good. Also, the expressive power of Ada is nice. I am really enjoying this.

---

Began work on the chapter on resolving and binding.

The importance of `aliased` finally dawned upon me. Consider the following the record:

    type A is record
        B : Integer;
        C : access Integer;
        D : aliased Integer;
    end record;

In this case, `B` is simply an integer, `C` is an access type (pointer) to an integer and `D` will always have a memory address (an alias). But `D` is not a pointer! it is simply guaranteed to have an address, meaning that `D'Access` is valid.

2022-09-25
----------

After a long hiatus I am back at it. Pleasantly surprised that it took almost no effort to get back into gears. In these few hours of effort, I started from several months break, implemented some missing features, found a nasty bug and fixed it.

The return statement has been implemented and it follows the Java implementation somewhat closely. Ada's exceptions can not contain data (they are not even objects). Instead, I added a function to the interpreter for getting the "return values" from outside of the interpreter. The rest of the mechanism was the same as jlox.

However, once I ran the Fibonacci number calculator, I immediately saw that the calculations were all wrong. I pinpointed the problem to be something do with variable environments. In the end, I implemented a new native function, which prints the content of all available environments. The hash map does not store the key value (why should it? That is what the hash is for), so a set of keys is not also part of the environment type.

This new helper function quickly revealed, that the problem was with unwinding function calls when return statements are involved. Since the return statement is implemented using an exception as a control flow mechanism, it skipped passed the environment unwinding of the function call. An additional exception handler was added to deal with this case.

Happy with the progress after such a long break.
2022-06-05
----------

Ran into a quite nasty bug in my implementation of variable scoping. The nastiness was in the misleading the nature of the error report, GNAT reported it as a `STORAGE_ERROR`. This indicated either a stack overflow or an erronous access. Since I worked with the build in `Containers` package, I assumed that erroneous access would not be a problem.

However, it seems like it was indeed the problem. There was not any protection against trying to use an uninitialized hash map. Not nice!

Thankfully, it did at least crash, instead of just behaving weirdly.

---

With the above fixed, I could finish up the first chunk of user-defined functions. They can now be defined and called.

2022-05-29
----------

Moved forward with implementing function declarations. Nothing particular to tell about.

2022-05-28
----------

Finished up function calling and the `clock` function. This was one of those places where it was not immediately obvious how to map the Java implementation to Ada.

The callable objects are implemented as records, which are then referenced using an access type in the `Value` record. This is not very elegant, but it works. This could probably be done better by implementing a proper object system instead of the record type I have now. I also had some issue with dispatching rules in Ada -- I can't say that I fully understand them, but at least it was quick to find a solution.

In general, I am in awe of how much there is to learn. There are so many facets of the Ada language: packages, visibility rules, details of the the type system. It is by no means lacking in concepts to explore.

Getting the calendar system to spit out a number of seconds was surprisingly tricky to figure out. It turns out the `Float` type is only with 6 digits of precision, which is way too little for a Unix timestamp. Increased precision on the internal number type to 10.

2022-05-27
----------

Started work on call parsing.

2022-05-16
----------

Implemented short-circuiting logical operators, while loops and for loops. Easy peasy!

I am getting to a point where Ada is not at all in the way of writing whatever I want. From here on I can start learning the more sophisticated concepts in Ada.

2022-05-15
----------

Finished up the implementation of the environments, which means that blcok scoping now works. This was a really interesting chapter, since it challenged my Ada implementation of the interpreter. This required reevaluation of choices and a solid clean-up.

The ability to easily work with containers demonstrate how big an improvement Ada 2005 was. Having it in the standard library pushes Ada from bare-bones to very, very workable.

The implementation environments does use access types at the moment, but think I would rather move on than dwell too much on it.

Moved on to implementing if-else statements, which did not have any particularly exciting details in terms of using Ada.

2022-05-12
----------

Block parsing and execution is coming along. There are a few loose ends which needs to be handled:

Currently there is only one variable environment and I need to figure out a good way to set them up and handle enclosing. Right now it is done using access types, but I am not sure it is the best method.

Execution of the interpreter needs to be move from `ada_lox.adb` to the interpreter itself. The `Execute` procedure is needed for the block execution too.

2022-05-08
----------

Implemented assignment and error handling around assignment. The most tricky part was how to handle polymorphism around fields in records, but was simply some syntax I had to look up. It occurs in the following code:`

```
function Assignment return Expr_Acc is
    Expr_Loc : Expr_Acc := Equality;
begin
    if Match (equal) then
      declare
          Equals    : Token    := Previous;
          Value_Loc : Expr_Acc := Assignment;
      begin
          if Expr_Loc.all in Expr.Variable then
            return new Assign'
                (Name  => Expr.Variable (Expr_Loc.all).Name,
                  Value => Value_Loc);
          end if;

          Error (Equals.Kind, "Invalid assignment target");
      end;
    end if;
```

Here `Expr_Loc` is an access type to a base type `Expr.Expr`. However, if it is of the type `Expr.Variable` (derived from `Expr.Expr`), then we must access the `Name` field. Turns out the syntax for a membership test is used (`in`) and the cast is done using `Expr.Variable (Expr_Loc.all)`. Easy enough once the right syntax was found.

2022-05-07
----------

The token type is now avoid access types too. Also renamed the package to have naming which is more consistent with the other packages.

2022-05-02
----------

I posted about my troubles with the access types and the Values package on r/Ada. It was pointed out that _mutable_ variant records are a thing and that these do solve my problem. I've now converted completely away from using access types for the Value records, which is really nice. Something similar could also be doing with the token types.

2022-05-01
----------

Got global variables to work, but it required that I made a named access type for values. I don't really like this solution -- it feels very awkward.

There are many concepts around access types which I understand well enough to somewhat use, but not at a depth that allow me to use them well.

2022-04-30
----------

I finally added the code generator for the AST instead of hand writing it. I cheated somewhat on this, since I wrote the code generator in Python instead of Ada -- but at least it is written.

Continued to work on variables. This turned out to be really tricky. Implementing the parsing and evaluation was simple enough, but the actual environment for storing the variable values is posing to be surprisingly complicated. The mix of pointers, indefinite types and such is really making it complicated for Ada to understand what is going on with the hashed map for the variables. Parsing and evaluation works, but the variables don't actually have any values assigned to them.

2022-04-24
----------

Statements! Expression and print statements are now implemented. This required that the visitor pattern was implemented using an interface, which turned out to be straightforward -- helpful error messages and the Ada 2005 rationale helped with understanding what to change and fix.

One statement I kinf of wish for would be an "assert" statement. It could help with testing the interpreter.

2022-04-23
----------

While working on runtime errors, the code felt like it got more and more disorganized and complex. Ada's exceptions are not really proper objects (records) like they are in Java. This means that I had to split up the error handling across multiple packages. When I came to implementing the error handling for the `+` operator, it all looked hopeless.

However, I realized that my `Value` package could handle the actual checking of the runtime values and the interpreter could then handling the reporting. This was a much more natural split and I think the code is better for it.

A further and interesting extension would be to use contracts to encode the value constraint.

Also to discuss some of the challenges:

- Comparison between different types: Ada is very strict on this point and it think can help in encoding constraints. However, comparison between f.x. floats and integers is definitely useful.
- Automatic string conversion: Kinda nasty, but here it goes: Check if either is a string. The `Image` attribute might be a bad choice for doing the conversion, so a string conversion helper should be implemented based upon `To_String`.
- Divide by zero: Ada returns `+Inf` for `1/0` which is according to standard. Implemented the runtime error because it feels stricter.

2022-04-17
----------

After a two week hiatus I am back. When I last worked on the code, I was trying to move the value representation to a proper value type in its own package. This was finished up so we now have proper support for booleans and nil too.

I then implemented comparison operators.

Coming back to the code after two weeks was not too difficult. It is hard to say if it is Ada or the fact that I am following a literal guide for implementing the interpreter -- it is most likely the latter. Ada at least does not make it unecessarily difficult; even while being a new language for me.

Made some progress on error reporting, but this is a place where Ada's and Java's idea of exceptions diverge. Ada's exceptions are not objects which can be instantiated, passed around or subclassed. They are exceptions with an optional message. jlox uses Java's exception model to attach information about the token to the thrown exception. This functionality will have to be captured somewhere else instead.

2022-04-03
----------

Finished the parser by adding the `Synchronize` procedure -- now it is time for actually evaluating expressions!

This has been real exciting even though it turned out to be quite simple. The interpreter actually _does_ something! Arithmetic on numbers and concatenating strings.

I use the tokens to represent values in the expression tree, which is rather annoying. I have to create a more lightweight value type, which can be used for this. Right now, I have to set up a lexeme string for all intermediate values, which is completely unecessary.
 
2022-03-29
----------

Finish most of the parser. The panic mode required that I use exceptions, which I then got an opportunity to learn about. Synchronization is still missing, but it should be rather trivial to implement.

2022-03-27
----------

Fixed the unary bug. It was a missing return statement, which caused the unary expressions to never termiate.

2022-03-22
----------

Fixed the `false` bug -- turned out to just be a spelling mistake.

The unary bug revealed that the short-circuit logical expressions have a different form in Ada. I remebered when I read it, but it was a nasty surprise.

2022-03-20
----------

Implemented most the initial parser. I tried to use subprogram-level functions instead of package-level functions. That is, instead of

```
package Parser is
  function Parse ...;

  function Advance ...;
  function Check ...;
end Parser;
```

the helper functions (`Advance`, `Check`) are subprograms of the `Parse` function. It is fairly clean, but it might not the most flexible design -- hopefully it is good enough for this interpreter.

There is a bug in the current parser. It seems to enter an infinite loop when encountering unary expressions or the `false` token. Very odd.

2022-03-19
----------

I think I am reasonably satisfied with the expression types and the implementation of the visitor pattern. I do not have the code generator as in the book, but the principle is there and for now I think I will just move forward to the next chapter on parsing expressions.

Implementing this expression tree and the visitor pattern was a success. It felt quite daunting at the beginning because my understanding of Ada did not map very well to the implementatin in Java. In the end it has only taken about 4 work sessions to tackle, which is very satisfactory.

2022-03-18
----------

As far as I can tell, it is not possible to have generic return values on the visitor pattern in Ada, since generic functions can not be overridden. Not totally, sure, but it makes some sense with how generics work in Ada.

I found another way to structure the visitor pattern instead, which also works. It is good enough to implement the AST printer and, from reading ahead, good enough for the scope resolver.

2022-03-16
----------

Use the AdaCore blog post about the visitor pattern to implement an initial implementation of the visitor pattern for the code tree representation.

One aspect, which I have not yet covered, is the generic return value. Either I have to figure out a way to implement this in Ada or else find another solution. I think it could be done using a generic package, but I am not completely sure.

2022-03-14
----------

Fixed the bug related to `1.0` not tokenizing properly. It was due to a wrongly done bounds check. The debugger was very helpful in tracking down the issue. The fix was finally implemented using exception handling in Ada -- it is nice to use some more features of the language.

Began work on the code representation chapter, but I am a bit stumped on how to tackle the visitor pattern in Ada. This article might help: https://www.adacore.com/gems/gem-113-visitor-pattern-in-ada 

2022-03-13
----------

Completed the scanner by implementing the keyword look up. Again, it was possible to lean quite closely to the Java implementation by using the hash map implementation from the `Containers` library.

There is a bug, which I think will be suitable for trying out the debugger. The string `1.0` is not tokenized correctly. It becomes `NUMBER DOT NUMBER` instead of just `NUMBER`. It is possibly related to some lookahead mechanics, which are wrong -- very suitable for debugging.

2022-03-12
----------

Fixed file reading to read in and process an entire source file instead line by line. Then continued to add string and number scanning, which was very uneventful. There were no nasty surprises or trickyness. Now that I have learned to navigate the Ada documentation, it is a lot easier to figure out where to find the right information regarding, for example, string handling.

2022-03-05
----------

Reworked the token record type such that the _value_ type is the discriminant, rather than the _token_ type. This reduces the number of variants to four (identifiers, strings, numbers and non-values) and thus the issue of statically declared variant records can be handled somewhat neatly.

Good and rather uneventful progress on the scanner. For some reason, I have made my implementation handle the scripts line by line, which is something which needs to be reworked.

2022-03-04
----------

Started work on the scanner proper. Ran into some problems related to variant records. Apparently, the discriminant has to be static (constant), which is not (?) possible when passing it as an argument.

I have to figure out how to work around this.

The structure of the scanner package is not something I am too happy with. It feels weird to pass the Scanner record around.

2022-02-24
----------

Improved token printing to match better with how it is done in the book. This required me to convert all string representations to unbounded strings before printing. I wonder how costly this conversion is and how much overhead is involved in this.

Did some initial work on setting up the scanner. More unbounded strings to make things work. The implementation effort was surprisingly smooth -- drew on some of my previous knowledge of access types in Ada to store the Token records in the token vector.

In the end, I hooked it all up to the actual CLI front-end. Great to see it moving forward bit by bit.

2022-02-22
----------

Started work on the token type definition and got it mostly set up. I decided to use bounded types for the lexeme and identifier strings, as I already knew that it would make sense to keep these bounded. Further, I also use a variant record for the token type instead of the generic `object` used in the book.

2022-02-16
----------

Finished the introduction to the Lox language and played around with it using [YALI.js](https://danman113.github.io/YALI.js/). 

```
// Quotient and modulus

fun quotient(a, b) {
  var i = 0;
  while (a >= b * i) {
    i = i + 1;
  }
  return i - 1;
}

fun modulus(a, b) {
  return a - b * quotient(a, b);
}

print modulus(9, 5);
```

Several cases around very large values are not defined.

One particular feature which is missing is a module system. Everything is single-file. There are also no arrays or maps.

Implemented the initial front end without too much trouble. Curious to see how mismatched some of my implementation assumptions are with the subsequent changes in the book.

2022-02-15
----------

Set up repository, figured out debugging in Ada and started reading on the Lox language.