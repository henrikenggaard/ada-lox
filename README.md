Ada implementation of the Lox interpreters
==========================================

Following [Crafting Interpreters](https://craftinginterpreters.com) closely.

Work ongoing and not at all done. Licensed as MIT, see `LICENSE` file.

Usage:

    alr build
    ./bin/ada_lox scripts/arithmetic.lox

Useful resources
----------------

- Reference manual
- [Ada wikibook](https://en.wikibooks.org/wiki/Ada_Programming)
- Rationale
  - [Ada 2012](http://www.ada-auth.org/standards/12rat/html/Rat12-TOC.html)
  - [Ada 2005](https://www.adaic.org/resources/add_content/standards/05rat/html/Rat-TOC.html)