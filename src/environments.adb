with Ada.Text_IO;

package body Environments is
   procedure Assign
     (E   : in out Environment;
      Key :        Lexeme_Str.Bounded_String;
      V   :        Values.Value)
   is
      S_Key : String := Lexeme_Str.To_String (Key);
   begin
      if E.Env.Contains (S_Key) then
         E.Env.Replace (S_Key, V);
         return;
      end if;

      if E.Enclosing /= null then
         E.Enclosing.Assign (Key, V);
         return;
      end if;

      raise Not_Declared_Error with "Undefined variable '" & S_Key & "'.";
   end Assign;

   procedure Assign_At
     (E     : in out Environment;
      Key   :        Lexeme_Str.Bounded_String;
      V     :        Values.Value;
      Depth :        Natural)
   is
   begin
      if E.Depth = Depth then
         E.Assign (Key, V);
      else
         E.Enclosing.Assign_At (Key, V, Depth);
      end if;
   end Assign_At;

   procedure Define
     (E   : in out Environment;
      Key :        Lexeme_Str.Bounded_String;
      V   :        Values.Value)
   is
      S_Key : String := Lexeme_Str.To_String (Key);
   begin
      E.Keys.Include (S_Key);
      E.Env.Include (S_Key, V);
   end Define;

   function Contains
     (E   : in out Environment;
      Key :        Lexeme_Str.Bounded_String) return Boolean
   is
   begin
      return E.Keys.Contains (Lexeme_Str.To_String (Key));
   end Contains;

   function Get
     (E   : in out Environment;
      Key :        Lexeme_Str.Bounded_String) return Values.Value
   is
      S_Key : String := Lexeme_Str.To_String (Key);
   begin
      if E.Env.Contains (S_Key) then
         return E.Env.Element (S_Key);
      end if;

      if E.Enclosing /= null then
         return E.Enclosing.Get (Key);
      end if;

      raise Not_Declared_Error with "Undefined variable '" & S_Key & "'.";
   end Get;

   function Get_At
     (E     : in out Environment;
      Key   :        Lexeme_Str.Bounded_String;
      Depth :        Natural) return Values.Value
   is
   begin
      if E.Depth = Depth then
         return E.Get (Key);
      else
         return E.Enclosing.Get_At (Key, Depth);
      end if;
   end Get_At;

   procedure Print (E : in out Environment) is
   begin
      for Key of E.Keys loop
         -- TODO: Figure out a way to print the content of an environment. The hash map does not actually know the actual strings, only their hash.
         Ada.Text_IO.Put (E.Depth'Image);
         Ada.Text_IO.Put (": ");
         Ada.Text_IO.Put (Key);
         Ada.Text_IO.Put (" = ");
         Ada.Text_IO.Put_Line
           (Values.To_String (E.Get (Lexeme_Str.To_Bounded_String (Key))));
      end loop;

      if E.Enclosing /= null then
         Ada.Text_IO.Put_Line ("--- enclosing ---");
         E.Enclosing.Print;
      end if;
   end Print;

   function Construct
     (Enclosing : access Environment) return access Environment
   is
      New_Env : access Environment := new Environment;
   begin
      New_Env.Enclosing := Enclosing;
      New_Env.Depth     := Enclosing.Depth + 1;
      return New_Env;
   end Construct;

end Environments;
