with Ada.Containers; use Ada.Containers;
with Ada.Containers.Indefinite_Hashed_Maps;
with Ada.Containers.Indefinite_Hashed_Sets;
with Ada.Strings.Hash;
with Tokens;         use Tokens;
with Errors;
with Values;

package Environments is

   Not_Declared_Error : exception;

   type Environment is tagged private;

   procedure Assign
     (E   : in out Environment;
      Key :        Lexeme_Str.Bounded_String;
      V   :        Values.Value);

   procedure Assign_At
     (E     : in out Environment;
      Key   :        Lexeme_Str.Bounded_String;
      V     :        Values.Value;
      Depth :        Natural);

   procedure Define
     (E   : in out Environment;
      Key :        Lexeme_Str.Bounded_String;
      V   :        Values.Value);

   function Contains
     (E   : in out Environment;
      Key :        Lexeme_Str.Bounded_String) return Boolean;

   function Get
     (E   : in out Environment;
      Key :        Lexeme_Str.Bounded_String) return Values.Value;

   function Get_At
     (E     : in out Environment;
      Key   :        Lexeme_Str.Bounded_String;
      Depth :        Natural) return Values.Value;

   procedure Print (E : in out Environment);

   function Construct
     (Enclosing : access Environment) return access Environment;
private
   package Map is new Ada.Containers.Indefinite_Hashed_Maps
     (Key_Type        => String,
      Element_Type    => Values.Value,
      Hash            => Ada.Strings.Hash,
      Equivalent_Keys => "=",
      "="             => Values."=");

   package Set is new Ada.Containers.Indefinite_Hashed_Sets
     (Element_Type        => String,
      Hash                => Ada.Strings.Hash,
      Equivalent_Elements => "=",
      "="                 => "=");

   type Environment is tagged record
      Enclosing : access Environment;
      Env       : Map.Map;
      Keys      : Set.Set;
      Depth     : Natural := 0;
   end record;
end Environments;
