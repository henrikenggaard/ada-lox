limited with Interpreter;
with Ada.Containers.Vectors;
with Values;

package Callables is

   type Argument_Number is range 0 .. 255;

   use type Values.Value;
   package Argument_Vectors is new Ada.Containers.Vectors
     (Index_Type   => Argument_Number,
      Element_Type => Values.Value);

   type Callable is interface;

   function Arity (Self : in out Callable) return Argument_Number is abstract;

   function Call
     (Self      : in out Callable;
      I         : in out Interpreter.Interpreter'Class;
      Arguments :    Argument_Vectors.Vector) return Values.Value is abstract;

   function Image (Self : in out Callable) return String is abstract;
end Callables;
