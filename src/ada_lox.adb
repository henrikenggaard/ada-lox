with Ada.Characters.Latin_1;
with Ada.Command_Line;
with Ada.Exceptions;
with Ada.Strings.Unbounded; use Ada.Strings.Unbounded;
with Ada.Text_IO;
with AST_Printer;           use AST_Printer;
with Errors;
with Interpreter;
with Parser;
with Resolver;
with Scanner;
with Stmts;
with Tokens;                use Tokens;

procedure Ada_Lox is
   package ACL renames Ada.Command_Line;
   package Text_IO renames Ada.Text_IO;

   Emit_Tokens : Boolean := False;
   Emit_AST    : Boolean := False;

   procedure Run (Line : String);

   function Read_Entire_File
     (Script : Ada.Text_IO.File_Type) return Unbounded_String;

   procedure Run_File (Path : String) is
      use Text_IO;
      Script : File_Type;
      Source : Unbounded_String;
   begin
      Open (Script, In_File, Path);
      Source := Read_Entire_File (Script);
      Run (To_String (Source));

      if Errors.Had_Runtime_Error then
         ACL.Set_Exit_Status (ACL.Exit_Status (70));
         return;
      end if;

      if Errors.Had_Error then
         ACL.Set_Exit_Status (ACL.Exit_Status (65));
         return;
      end if;
   end Run_File;

   procedure Run_Prompt is
      use Text_IO;
   begin
      Put (">> ");
      while not (End_Of_File) loop
         declare
            Line : String := Get_Line;
         begin
            Run (Line);
            Errors.Reset_Error;
            Put (">> ");
            Flush;
         end;
      end loop;
   end Run_Prompt;

   procedure Run (Line : String) is
      The_Scanner : Scanner.Scanner;
      Tokens      : Token_Vectors.Vector;
      Statements  : Stmts.Stmt_Vectors.Vector;

      Gen  : aliased Printer;
      Exec : aliased Interpreter.Interpreter;
      Res  : aliased Resolver.Resolver := (Exec => Exec);
   begin
      The_Scanner.Source := To_Unbounded_String (Line);
      Tokens             := The_Scanner.Scan_Tokens;

      if Emit_Tokens then
         for tkn of Tokens loop
            Text_IO.Put_Line (Image (tkn));
         end loop;
      end if;

      Statements := Parser.Parse (Tokens);

      if Errors.Had_Error then
         return;
      end if;

      for S of Statements loop
         S.Acceptor (Res'Access);
      end loop;

      if Errors.Had_Error then
         return;
      end if;

      if Emit_AST then
         for S of Statements loop
            S.Acceptor (Gen'Access);
         end loop;
         Ada.Text_IO.Put_Line (To_String (AST_Output));
      end if;

      Interpreter.Interpret (Exec, Statements);
   end Run;

   function Read_Entire_File
     (Script : Ada.Text_IO.File_Type) return Unbounded_String
   is
      use Ada.Text_IO;
      package Latin_1 renames Ada.Characters.Latin_1;
      Content : Unbounded_String;
   begin
      while not (End_Of_File (Script)) loop
         declare
            Line : String := Get_Line (Script);
         begin
            Append (Content, To_Unbounded_String (Line));
            Append (Content, To_Unbounded_String ("" & Latin_1.LF));
         end;
      end loop;

      return Content;
   end Read_Entire_File;

begin

   case ACL.Argument_Count is
      when 0 =>
         Run_Prompt;
      when 1 =>
         Run_File (ACL.Argument (1));
      when others =>
         Text_IO.Put_Line ("Usage ada_lox [script]");
         ACL.Set_Exit_Status (ACL.Exit_Status (64));
   end case;
end Ada_Lox;
