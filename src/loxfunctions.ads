limited with Interpreter;
limited with LoxInstance;
with Callables;
with Environments;
with Stmts;
with Tokens;
with Values;

package LoxFunctions is

   type Function_Kind is (Normal, Initializer);

   type LoxFunction is new Callables.Callable with record
      Name       : Tokens.Token;
      Parameters : Tokens.Token_Vectors.Vector;
      Statements : Stmts.Stmt_Vectors.Vector;
      Closure    : access Environments.Environment;
      Kind       : Function_Kind := Normal;
   end record;

   function Arity (Self : in out LoxFunction) return Callables.Argument_Number;

   function Bind
     (Original : LoxFunction;
      Instance : access LoxInstance.Instance) return access LoxFunction;

   function Call
     (Self      : in out LoxFunction;
      I         : in out Interpreter.Interpreter'Class;
      Arguments :    Callables.Argument_Vectors.Vector) return Values.Value;

   function Image (Self : in out LoxFunction) return String;
end LoxFunctions;
