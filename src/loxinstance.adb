with Tokens;
with LoxFunctions;
with Callables;

with Ada.Text_IO; use Ada.Text_IO;
package body LoxInstance is
   function Construct (Class : LoxClass.LoxClass) return access Instance is
   begin
      return new LoxInstance.Instance'
          (Class      => Class,
           Properties => new Environments.Environment,
           Methods    => Environments.Construct (Class.Methods));
   end Construct;

   function Image (Self : in out Instance) return String is
   begin
      return "<instance " &
        Tokens.Lexeme_Str.To_String (Self.Class.Name.Lexeme) &
        ">";
   end Image;

   function Get
     (Self : in out Instance;
      Key  :        Tokens.Lexeme_Str.Bounded_String) return Values.Value
   is
      Method_Value : Values.Value;
      use LoxFunctions;
   begin
      if Self.Properties.Contains (Key) then
         return Self.Properties.Get (Key);
      end if;

      begin
         Method_Value := Self.Methods.Get (Key);
      exception
         when E : Environments.Not_Declared_Error =>
            if Self.Class.Superclass /= null then
               Method_Value := Self.Class.Superclass.Methods.Get (Key);
            end if;
      end;

      Method_Value.Callee :=
        Bind (LoxFunction (Method_Value.Callee.all), Self'Access);
      return Method_Value;
   end Get;

   procedure Set
     (Self : in out Instance;
      Key  :        Tokens.Lexeme_Str.Bounded_String;
      V    :        Values.Value)
   is
   begin
      Self.Properties.Define (Key, V);
   end Set;
end LoxInstance;
