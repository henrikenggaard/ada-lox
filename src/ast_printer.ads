with Ada.Strings.Unbounded; use Ada.Strings.Unbounded;
with Exprs;
with Exprs_Visitor;
with Stmts;
with Stmts_Visitor;

package AST_Printer is
   AST_Output : Unbounded_String;

   type Printer is new Exprs_Visitor.Visitor and
     Stmts_Visitor.Visitor with
   null record;

   overriding procedure Visit_Assign
     (Self : in out Printer;
      Obj  : in out Exprs.Assign'Class);

   overriding procedure Visit_Binary
     (Self : in out Printer;
      Obj  : in out Exprs.Binary'Class);

   overriding procedure Visit_Call
     (Self : in out Printer;
      Obj  : in out Exprs.Call'Class);

   overriding procedure Visit_Get
     (Self : in out Printer;
      Obj  : in out Exprs.Get'Class);

   overriding procedure Visit_Grouping
     (Self : in out Printer;
      Obj  : in out Exprs.Grouping'Class);

   overriding procedure Visit_Literal
     (Self : in out Printer;
      Obj  : in out Exprs.Literal'Class);

   overriding procedure Visit_Logical
     (Self : in out Printer;
      Obj  : in out Exprs.Logical'Class);

   overriding procedure Visit_Set
     (Self : in out Printer;
      Obj  : in out Exprs.Set'Class);

   overriding procedure Visit_Super
     (Self : in out Printer;
      Obj  : in out Exprs.Super'Class);

   overriding procedure Visit_This
     (Self : in out Printer;
      Obj  : in out Exprs.This'Class);

   overriding procedure Visit_Unary
     (Self : in out Printer;
      Obj  : in out Exprs.Unary'Class);

   overriding procedure Visit_Variable
     (Self : in out Printer;
      Obj  : in out Exprs.Variable'Class);

   overriding procedure Visit_Block
     (Self : in out Printer;
      Obj  : in out Stmts.Block'Class);

   overriding procedure Visit_Class
     (Self : in out Printer;
      Obj  : in out Stmts.Class'Class);

   overriding procedure Visit_Expression
     (Self : in out Printer;
      Obj  : in out Stmts.Expression'Class);

   overriding procedure Visit_Function_S
     (Self : in out Printer;
      Obj  : in out Stmts.Function_S'Class);

   overriding procedure Visit_If_S
     (Self : in out Printer;
      Obj  : in out Stmts.If_S'Class);

   overriding procedure Visit_Print
     (Self : in out Printer;
      Obj  : in out Stmts.Print'Class);

   overriding procedure Visit_Var
     (Self : in out Printer;
      Obj  : in out Stmts.Var'Class);

   overriding procedure Visit_Return_S
     (Self : in out Printer;
      Obj  : in out Stmts.Return_S'Class);

   overriding procedure Visit_While_S
     (Self : in out Printer;
      Obj  : in out Stmts.While_S'Class);
private
end AST_Printer;
