with Ada.Text_IO;
with Tokens;

package Errors is
   Runtime_Error : exception;

   Had_Runtime_Error : Boolean := False;

   function Had_Error return Boolean;

   procedure Reset_Error;

   function Error_Msg
     (Line_No : Tokens.Line_Number;
      Msg     : String;
      Where   : String := "") return String;

   procedure Put_Error
     (Line_No : Tokens.Line_Number;
      Msg     : String;
      Where   : String := "");

private
   prv_Had_Error : Boolean := False;
end Errors;
