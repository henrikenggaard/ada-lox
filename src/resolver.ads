with Exprs_Visitor;
with Exprs;
with Interpreter;
with Stmts_Visitor;
with Stmts;

package Resolver is
   type Resolver is new Exprs_Visitor.Visitor and
     Stmts_Visitor.Visitor with
   record
      Exec : Interpreter.Interpreter;
   end record;

   overriding procedure Visit_Assign
     (Self : in out Resolver;
      Obj  : in out Exprs.Assign'Class);

   overriding procedure Visit_Binary
     (Self : in out Resolver;
      Obj  : in out Exprs.Binary'Class);

   overriding procedure Visit_Call
     (Self : in out Resolver;
      Obj  : in out Exprs.Call'Class);

   overriding procedure Visit_Get
     (Self : in out Resolver;
      Obj  : in out Exprs.Get'Class);

   overriding procedure Visit_Grouping
     (Self : in out Resolver;
      Obj  : in out Exprs.Grouping'Class);

   overriding procedure Visit_Literal
     (Self : in out Resolver;
      Obj  : in out Exprs.Literal'Class);

   overriding procedure Visit_Logical
     (Self : in out Resolver;
      Obj  : in out Exprs.Logical'Class);

   overriding procedure Visit_Set
     (Self : in out Resolver;
      Obj  : in out Exprs.Set'Class);

   overriding procedure Visit_Super
     (Self : in out Resolver;
      Obj  : in out Exprs.Super'Class);

   overriding procedure Visit_This
     (Self : in out Resolver;
      Obj  : in out Exprs.This'Class);

   overriding procedure Visit_Unary
     (Self : in out Resolver;
      Obj  : in out Exprs.Unary'Class);

   overriding procedure Visit_Variable
     (Self : in out Resolver;
      Obj  : in out Exprs.Variable'Class);

   overriding procedure Visit_Block
     (Self : in out Resolver;
      Obj  : in out Stmts.Block'Class);

   overriding procedure Visit_Class
     (Self : in out Resolver;
      Obj  : in out Stmts.Class'Class);

   overriding procedure Visit_Expression
     (Self : in out Resolver;
      Obj  : in out Stmts.Expression'Class);

   overriding procedure Visit_Function_S
     (Self : in out Resolver;
      Obj  : in out Stmts.Function_S'Class);

   overriding procedure Visit_If_S
     (Self : in out Resolver;
      Obj  : in out Stmts.If_S'Class);

   overriding procedure Visit_Print
     (Self : in out Resolver;
      Obj  : in out Stmts.Print'Class);

   overriding procedure Visit_Var
     (Self : in out Resolver;
      Obj  : in out Stmts.Var'Class);

   overriding procedure Visit_Return_S
     (Self : in out Resolver;
      Obj  : in out Stmts.Return_S'Class);

   overriding procedure Visit_While_S
     (Self : in out Resolver;
      Obj  : in out Stmts.While_S'Class);
private
end Resolver;
