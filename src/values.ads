with Ada.Strings.Unbounded; use Ada.Strings.Unbounded;
with Ada.Containers.Vectors;
limited with Callables;
limited with LoxInstance;

package Values is
   Value_Error : exception;

   type Value_Enum is (boolean_k, callable, instance, nil, number, string_k);

   type LoxNumber is digits 10;

   type Value (Kind : Value_Enum := nil) is record
      case Kind is
         when boolean_k =>
            Bool_V : Boolean;
         when callable =>
            Callee : access Callables.Callable'Class;
         when instance =>
            I : access LoxInstance.Instance;
         when nil =>
            null;
         when number =>
            Number : LoxNumber;
         when string_k =>
            String : Unbounded_String;
      end case;
   end record;

   package Value_Vectors is new Ada.Containers.Vectors
     (Index_Type   => Natural,
      Element_Type => Value);

   function "not" (V : Value) return Value;

   function "=" (A, B : Value) return Value;

   function "/=" (A, B : Value) return Value;

   function "<" (A, B : Value) return Value;

   function "<=" (A, B : Value) return Value;

   function ">" (A, B : Value) return Value;

   function ">=" (A, B : Value) return Value;

   function "+" (A, B : Value) return Value;

   function "-" (V : Value) return Value;

   function "-" (A, B : Value) return Value;

   function "*" (A, B : Value) return Value;

   function "/" (A, B : Value) return Value;

   function To_Bool (V : Value) return Value;

   function To_Ada_Bool (V : Value) return Boolean;

   function To_String (V : Value) return String;
end Values;
