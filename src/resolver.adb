with Ada.Containers.Indefinite_Hashed_Maps;
with Ada.Containers.Vectors;
with Ada.Strings.Hash;
with Tokens;
with Exprs;
with Errors;
use type Exprs.Expr_Acc;

package body Resolver is

   type Var_State is (Unintialized, Initialized);

   type Class_Type is (None, Class, Subclass);

   Current_Class : Class_Type := None;

   type Function_Type is (None, Func, Method, Initializer);

   Current_Function : Function_Type := None;

   package Scope is new Ada.Containers.Indefinite_Hashed_Maps
     (Key_Type        => String,
      Element_Type    => Var_State,
      Hash            => Ada.Strings.Hash,
      Equivalent_Keys => "=",
      "="             => "=");

   package Scopes_Vec is new Ada.Containers.Vectors
     (Index_Type   => Positive,
      Element_Type => Scope.Map,
      "="          => Scope."=");

   Scopes : Scopes_Vec.Vector;

   procedure Begin_Scope is
      New_Scope : Scope.Map;
   begin
      Scopes.Append (New_Scope);
   end Begin_Scope;

   procedure End_Scope is
   begin
      Scopes.Delete_Last;
   end End_Scope;

   procedure Declare_Name (Name : Tokens.Token) is
   begin
      if Scopes.Is_Empty then
         return;
      end if;

      declare
         Current_Scope : Scope.Map := Scopes.Last_Element;
      begin
         if Current_Scope.Contains
           (Tokens.Lexeme_Str.To_String (Name.Lexeme))
         then
            Errors.Put_Error
              (Name.Line,
               "Already a variable with this name in this scope.");
         end if;
         Current_Scope.Include
         (Tokens.Lexeme_Str.To_String (Name.Lexeme), Unintialized);
-- TODO: We have to replace the map after having modified it. That is not nice.
         Scopes.Replace_Element (Scopes.Last_Index, Current_Scope);
      end;
   end Declare_Name;

   procedure Define (Name : Tokens.Lexeme_Str.Bounded_String) is
   begin
      if Scopes.Is_Empty then
         return;
      end if;

      declare
         Current_Scope : Scope.Map := Scopes.Last_Element;
      begin
         Current_Scope.Include
         (Tokens.Lexeme_Str.To_String (Name), Initialized);
-- TODO: We have to replace the map after having modified it. That is not nice.
         Scopes.Replace_Element (Scopes.Last_Index, Current_Scope);
      end;
   end Define;

   procedure Resolve (Self : in out Resolver; Expr : Exprs.Expr_Acc) is
   begin
      Expr.Acceptor (Self'Access);
   end Resolve;

   procedure Resolve (Self : in out Resolver; Statement : Stmts.Stmt_Acc) is
   begin
      Statement.Acceptor (Self'Access);
   end Resolve;

   procedure Resolve
     (Self       : in out Resolver;
      Statements :        Stmts.Stmt_Vectors.Vector)
   is
   begin
      for Statement of Statements loop
         Self.Resolve (Statement);
      end loop;
   end Resolve;

   procedure Resolve_Local
     (LExpr : in out Exprs.Expr'Class;
      Name  :        Tokens.Lexeme_Str.Bounded_String)
   is
      LName : String := Tokens.Lexeme_Str.To_String (Name);

      procedure Set_Depth (Depth : Natural) is
      begin
         if LExpr in Exprs.Variable'Class then
            Exprs.Variable (LExpr).Depth := Depth;
         elsif LExpr in Exprs.Assign'Class then
            Exprs.Assign (LExpr).Depth := Depth;
         elsif LExpr in Exprs.Super'Class then
            Exprs.Super (LExpr).Depth := Depth;
         end if;
      end Set_Depth;
   begin
      for i in reverse Scopes.First_Index .. Scopes.Last_Index loop
         if Scopes.Element (i).Contains (LName) then
            Set_Depth (i);
            return;
         end if;
      end loop;
      -- TODO: Might not be necessary
      Set_Depth (0);
   end Resolve_Local;

   procedure Resolve_Function
     (Self  : in out Resolver;
      Func  :        Stmts.Function_S'Class;
      FType :        Function_Type)
   is
      Enclosing_FType : Function_Type := Current_Function;
   begin
      Current_Function := FType;
      Begin_Scope;
      for param of Func.Parameters loop
         Declare_Name (param);
         Define (param.Lexeme);
      end loop;
      Self.Resolve (Func.Statements);
      End_Scope;
      Current_Function := Enclosing_FType;
   end Resolve_Function;

   overriding procedure Visit_Assign
     (Self : in out Resolver;
      Obj  : in out Exprs.Assign'Class)
   is
   begin
      Self.Resolve (Obj.Value);
      Resolve_Local (Obj, Obj.Name.Lexeme);
   end Visit_Assign;

   overriding procedure Visit_Binary
     (Self : in out Resolver;
      Obj  : in out Exprs.Binary'Class)
   is
   begin
      Self.Resolve (Obj.Left);
      Self.Resolve (Obj.Right);
   end Visit_Binary;

   overriding procedure Visit_Call
     (Self : in out Resolver;
      Obj  : in out Exprs.Call'Class)
   is
   begin
      Self.Resolve (Obj.Callee);

      for Argument of Obj.Arguments loop
         Self.Resolve (Argument);
      end loop;
   end Visit_Call;

   overriding procedure Visit_Get
     (Self : in out Resolver;
      Obj  : in out Exprs.Get'Class)
   is
   begin
      Obj.Object.Acceptor (Self'Access);
   end Visit_Get;

   overriding procedure Visit_Grouping
     (Self : in out Resolver;
      Obj  : in out Exprs.Grouping'Class)
   is
   begin
      Self.Resolve (Obj.Expression);
   end Visit_Grouping;

   overriding procedure Visit_Literal
     (Self : in out Resolver;
      Obj  : in out Exprs.Literal'Class)
   is
   begin
      null;
   end Visit_Literal;

   overriding procedure Visit_Logical
     (Self : in out Resolver;
      Obj  : in out Exprs.Logical'Class)
   is
   begin
      Self.Resolve (Obj.Left);
      Self.Resolve (Obj.Right);
   end Visit_Logical;

   overriding procedure Visit_Set
     (Self : in out Resolver;
      Obj  : in out Exprs.Set'Class)
   is
   begin
      Self.Resolve (Obj.Value);
      Self.Resolve (Obj.Object);
   end Visit_Set;

   overriding procedure Visit_Super
     (Self : in out Resolver;
      Obj  : in out Exprs.Super'Class)
   is
   begin
      case Current_Class is
         when None =>
            Errors.Put_Error
              (Obj.Keyword.Line,
               "Can't use 'super' outside of class.");
         when Class =>
            Errors.Put_Error
              (Obj.Keyword.Line,
               "Can't use 'super' in class with no superclass.");
         when Subclass =>
            Resolve_Local (Obj, Obj.Keyword.Lexeme);
      end case;
   end Visit_Super;

   overriding procedure Visit_This
     (Self : in out Resolver;
      Obj  : in out Exprs.This'Class)
   is
   begin
      if Current_Class = None then
         Errors.Put_Error
           (Obj.Keyword.Line,
            "Can't use 'this' outside of class.");
      end if;
      Resolve_Local (Obj, Obj.Keyword.Lexeme);
   end Visit_This;

   overriding procedure Visit_Unary
     (Self : in out Resolver;
      Obj  : in out Exprs.Unary'Class)
   is
   begin
      Self.Resolve (Obj.Right);
   end Visit_Unary;

   overriding procedure Visit_Variable
     (Self : in out Resolver;
      Obj  : in out Exprs.Variable'Class)
   is
   begin
      if not Scopes.Is_Empty
        and then Scopes.Last_Element.Contains
        (Tokens.Lexeme_Str.To_String (Obj.Name.Lexeme))
      then
         if Scopes.Last_Element.Element
           (Tokens.Lexeme_Str.To_String (Obj.Name.Lexeme)) =
           Unintialized
         then
            Errors.Put_Error
              (Obj.Name.Line,
               "Can't read local variable in its own initializer.");
         end if;
      end if;

      Resolve_Local (Obj, Obj.Name.Lexeme);
   end Visit_Variable;

   overriding procedure Visit_Block
     (Self : in out Resolver;
      Obj  : in out Stmts.Block'Class)
   is
   begin
      Begin_Scope;
      Self.Resolve (Obj.Statements);
      End_Scope;
   end Visit_Block;

   overriding procedure Visit_Class
     (Self : in out Resolver;
      Obj  : in out Stmts.Class'Class)
   is
      Enclosing_Class : Class_Type := Current_Class;
   begin
      Current_Class := Class;

      Declare_Name (Obj.Name);
      Define (Obj.Name.Lexeme);

      if Obj.Superclass /= null then
         Current_Class := Subclass;
         declare
            lcl : Exprs.Variable := Exprs.Variable (Obj.Superclass.all);
            use type Tokens.Lexeme_Str.Bounded_String;
         begin
            if Obj.Name.Lexeme = lcl.Name.Lexeme then
               Errors.Put_Error
                 (lcl.Name.Line,
                  "A class can't inherit from itself.");
            end if;
         end;

         Self.Resolve (Obj.Superclass);
      end if;

      if Obj.Superclass /= null then
         Begin_Scope;
         declare
            Current_Scope : Scope.Map := Scopes.Last_Element;
         begin
            Current_Scope.Include ("super", Initialized);
-- TODO: We have to replace the map after having modified it. That is not nice.
            Scopes.Replace_Element (Scopes.Last_Index, Current_Scope);
         end;
      end if;

      Begin_Scope;
      declare
         Current_Scope : Scope.Map := Scopes.Last_Element;
      begin
         Current_Scope.Include ("this", Initialized);
-- TODO: We have to replace the map after having modified it. That is not nice.
         Scopes.Replace_Element (Scopes.Last_Index, Current_Scope);
      end;

      for M of Obj.Methods loop
         declare
            use Tokens.Lexeme_Str;

            Kind   : Function_Type    := Method;
            Method : Stmts.Function_S := Stmts.Function_S (M.all);
         begin
            if Method.Name.Lexeme = "init" then
               Kind := Initializer;
            end if;

            Self.Resolve_Function (Method, Kind);
         end;
      end loop;

      End_Scope;

      if Obj.Superclass /= null then
         End_Scope;
      end if;

      Current_Class := Enclosing_Class;
   end Visit_Class;

   overriding procedure Visit_Expression
     (Self : in out Resolver;
      Obj  : in out Stmts.Expression'Class)
   is
   begin
      Self.Resolve (Obj.Ex);
   end Visit_Expression;

   overriding procedure Visit_Function_S
     (Self : in out Resolver;
      Obj  : in out Stmts.Function_S'Class)
   is
   begin
      Declare_Name (Obj.Name);
      Define (Obj.Name.Lexeme);

      Self.Resolve_Function (Obj, Func);
   end Visit_Function_S;

   overriding procedure Visit_If_S
     (Self : in out Resolver;
      Obj  : in out Stmts.If_S'Class)
   is
      use type Stmts.Stmt_Acc;
   begin
      Self.Resolve (Obj.Condition);
      Self.Resolve (Obj.Then_Branch);
      if Obj.Else_Branch /= null then
         Self.Resolve (Obj.Else_Branch);
      end if;
   end Visit_If_S;

   overriding procedure Visit_Print
     (Self : in out Resolver;
      Obj  : in out Stmts.Print'Class)
   is
   begin
      Self.Resolve (Obj.Ex);
   end Visit_Print;

   overriding procedure Visit_Var
     (Self : in out Resolver;
      Obj  : in out Stmts.Var'Class)
   is
   begin
      Declare_Name (Obj.Name);
      if Obj.Initializer /= null then
         Self.Resolve (Obj.Initializer);
      end if;
      Define (Obj.Name.Lexeme);
   end Visit_Var;

   overriding procedure Visit_Return_S
     (Self : in out Resolver;
      Obj  : in out Stmts.Return_S'Class)
   is
      use type Exprs.Expr_Acc;
   begin
      if Current_Function = None then
         Errors.Put_Error
           (Obj.keyword.Line,
            "Can't return from top-level code.");
      end if;

      if Obj.Value /= null then
         if Current_Function = Initializer then
            Errors.Put_Error
              (Obj.keyword.Line,
               "Can't return a value from an initializer.");
         end if;

         Self.Resolve (Obj.Value);
      end if;
   end Visit_Return_S;

   overriding procedure Visit_While_S
     (Self : in out Resolver;
      Obj  : in out Stmts.While_S'Class)
   is
   begin
      Self.Resolve (Obj.Condition);
      Self.Resolve (Obj.Loop_Body);
   end Visit_While_S;
end Resolver;
