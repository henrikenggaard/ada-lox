with Environments;
with Exprs_Visitor;
with Exprs; use Exprs;
with Stmts_Visitor;
with Stmts; use Stmts;
with Tokens;
with Values;

package Interpreter is
   Runtime_Error : exception;

   Return_Control_Flow : exception;

   type Interpreter is new Exprs_Visitor.Visitor and
     Stmts_Visitor.Visitor with
   null record;

   overriding procedure Visit_Assign
     (Self : in out Interpreter;
      Obj  : in out Exprs.Assign'Class);

   overriding procedure Visit_Binary
     (Self : in out Interpreter;
      Obj  : in out Exprs.Binary'Class);

   overriding procedure Visit_Call
     (Self : in out Interpreter;
      Obj  : in out Exprs.Call'Class);

   overriding procedure Visit_Get
     (Self : in out Interpreter;
      Obj  : in out Exprs.Get'Class);

   overriding procedure Visit_Grouping
     (Self : in out Interpreter;
      Obj  : in out Exprs.Grouping'Class);

   overriding procedure Visit_Literal
     (Self : in out Interpreter;
      Obj  : in out Exprs.Literal'Class);

   overriding procedure Visit_Logical
     (Self : in out Interpreter;
      Obj  : in out Exprs.Logical'Class);

   overriding procedure Visit_Set
     (Self : in out Interpreter;
      Obj  : in out Exprs.Set'Class);

   overriding procedure Visit_Super
     (Self : in out Interpreter;
      Obj  : in out Exprs.Super'Class);

   overriding procedure Visit_This
     (Self : in out Interpreter;
      Obj  : in out Exprs.This'Class);

   overriding procedure Visit_Unary
     (Self : in out Interpreter;
      Obj  : in out Exprs.Unary'Class);

   overriding procedure Visit_Variable
     (Self : in out Interpreter;
      Obj  : in out Exprs.Variable'Class);

   overriding procedure Visit_Block
     (Self : in out Interpreter;
      Obj  : in out Stmts.Block'Class);

   overriding procedure Visit_Class
     (Self : in out Interpreter;
      Obj  : in out Stmts.Class'Class);

   overriding procedure Visit_Expression
     (Self : in out Interpreter;
      Obj  : in out Stmts.Expression'Class);

   overriding procedure Visit_Function_S
     (Self : in out Interpreter;
      Obj  : in out Stmts.Function_S'Class);

   overriding procedure Visit_If_S
     (Self : in out Interpreter;
      Obj  : in out Stmts.If_S'Class);

   overriding procedure Visit_Print
     (Self : in out Interpreter;
      Obj  : in out Stmts.Print'Class);

   overriding procedure Visit_Var
     (Self : in out Interpreter;
      Obj  : in out Stmts.Var'Class);

   overriding procedure Visit_Return_S
     (Self : in out Interpreter;
      Obj  : in out Stmts.Return_S'Class);

   overriding procedure Visit_While_S
     (Self : in out Interpreter;
      Obj  : in out Stmts.While_S'Class);

   function To_String return String;

   procedure Execute_Block
     (Self       : in out Interpreter;
      Statements :        Stmts.Stmt_Vectors.Vector;
      Env        :        access Environments.Environment);

   procedure Interpret
     (Self       : in out Interpreter;
      Statements :        Stmts.Stmt_Vectors.Vector);

   Globals : access Environments.Environment;

   function Get_Return_Value return Values.Value;

private
   Return_Value : Values.Value;
   -- Find a better name for this
   Own : access Environments.Environment;
end Interpreter;
