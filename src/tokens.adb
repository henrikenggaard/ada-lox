package body Tokens is
   function Image (T : Token) return String is
      Literal : Unbounded_String;
      Lexeme  : String := Lexeme_Str.To_String (T.Lexeme);
   begin
      case T.Value_Kind is
         when string_k =>
            Literal := T.String;
         when number =>
            Literal := To_Unbounded_String (T.Number'Image);
         when others =>
            Literal := To_Unbounded_String ("");
      end case;
      return T.Kind'Image & " " & Lexeme & " " & To_String (Literal);
   end Image;
end Tokens;
