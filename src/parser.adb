with Callables;
with Stmts;  use Stmts;
with Values; use Values;

package body Parser is
   function Parse
     (Token_Vector : Token_Vectors.Vector) return Stmts.Stmt_Vectors.Vector
   is

      Statements : Stmts.Stmt_Vectors.Vector;

      Parse_Error : exception;

      Current : Natural := 0;

      procedure Error (Kind : Token_Enum; Msg : String);
      procedure Synchronize;

      function Previous return Token is
      begin
         return Token_Vector.Element (Current - 1);
      end Previous;

      function Peek return Token is
      begin
         return Token_Vector.Element (Current);
      end Peek;

      function Is_At_End return Boolean is
      begin
         return Peek.Kind = eof;
      end Is_At_End;

      procedure Advance is
      begin
         if Is_At_End then
            return;
         end if;

         Current := Current + 1;
      end Advance;

      function Check (Kind : Token_Enum) return Boolean is
      begin
         if Is_At_End then
            return False;
         end if;

         return Peek.Kind = Kind;
      end Check;

      function Match (Kind : Token_Enum) return Boolean is
      begin
         if Check (Kind) then
            Advance;
            return True;
         end if;

         return False;
      end Match;

      procedure Consume (Kind : Token_Enum; Msg : String) is
      begin
         if Check (Kind) then
            Advance;
            return;
         end if;

         Error (Kind, Msg);
      end Consume;

      function Expression return Expr_Acc;

      function Primary return Expr_Acc is
         Expr_Loc : Expr_Acc;
      begin

         if Match (number) then
            return new Literal'
                (Value => Values.Value'(number, Previous.Number));
         elsif Match (string_k) then
            return new Literal'
                (Value => Values.Value'(string_k, Previous.String));
         elsif Match (nil) then
            return new Literal'(Value => Values.Value'(Kind => nil));
         elsif Match (true) then
            return new Literal'(Value => Values.Value'(boolean_k, True));
         elsif Match (false) then
            return new Literal'(Value => Values.Value'(boolean_k, False));
         end if;

         if Match (Tokens.super) then
            declare
               Keyword : Tokens.Token := Previous;
               Method  : Tokens.Token;
            begin
               Consume (Tokens.dot, "Expect '.' after 'super'.");
               Consume (Tokens.identifier, "Expect superclass method name.");
               Method := Previous;
               return new Exprs.Super'(Keyword, Method, Depth => 0);
            end;
         end if;

         if Match (Tokens.this) then
            return new Exprs.This'(Keyword => Previous);
         end if;

         if Match (identifier) then
            return new Exprs.Variable'(Name => Previous, Depth => 0);
         end if;

         if Match (left_paren) then
            Expr_Loc := Expression;
            Consume (right_paren, "Expect ')' after expression");
            return new Grouping'(Expression => Expr_Loc);
         end if;

         Error (Peek.Kind, "Expect expression");
      end Primary;

      function Finish_Call (Callee : Expr_Acc) return Expr_Acc;

      function Find_Call return Expr_Acc is
         Expr_Loc : Expr_Acc := Primary;
         Name     : Tokens.Token;
      begin

         Call_Parse_Loop :
         while True loop
            if Match (left_paren) then
               Expr_Loc := Finish_Call (Expr_Loc);
            elsif Match (dot) then
               Consume (identifier, "Expect property name after '.'.");
               Name     := Previous;
               Expr_Loc := new Exprs.Get'(Object => Expr_Loc, Name => Name);
            else
               exit Call_Parse_Loop;
            end if;
         end loop Call_Parse_Loop;

         return Expr_Loc;
      end Find_Call;

      function Finish_Call (Callee : Expr_Acc) return Expr_Acc is
         Arguments : Expr_Vectors.Vector;
         use Ada.Containers;
      begin
         if not Check (right_paren) then
            Argument_Loop :
            loop
               if Arguments.Length >=
                 Count_Type (Callables.Argument_Number'Last)
               then
                  Error
                    (Peek.Kind,
                     "Can't have more than" &
                     Callables.Argument_Number'Last'Image &
                     " arguments.");
               end if;
               Arguments.Append (Expression);
               exit Argument_Loop when not Match (comma);
            end loop Argument_Loop;
         end if;

         Consume (right_paren, "Expect ')' after arguments.");
         declare
            Paren : Token := Previous;
         begin
            return new Exprs.Call'
                (Callee => Callee, paren => Paren, Arguments => Arguments);
         end;

      end Finish_Call;

      function Find_Unary return Expr_Acc is
         Operator : Token;
         Right    : Expr_Acc;
      begin
         if Match (bang) or else Match (minus) then
            Operator := Previous;
            Right    := Find_Unary;
            return new Unary'(Operator => Operator, Right => Right);
         end if;

         return Find_Call;
      end Find_Unary;

      function Factor return Expr_Acc is
         Expr_Loc : Expr_Acc := Find_Unary;
         Operator : Token;
         Right    : Expr_Acc;
      begin
         while Match (star) or else Match (slash) loop
            Operator := Previous;
            Right    := Find_Unary;
            Expr_Loc :=
              new Binary'
                (Left => Expr_Loc, Operator => Operator, Right => Right);
         end loop;

         return Expr_Loc;
      end Factor;

      function Term return Expr_Acc is
         Expr_Loc : Expr_Acc := Factor;
         Operator : Token;
         Right    : Expr_Acc;
      begin
         while Match (minus) or else Match (plus) loop
            Operator := Previous;
            Right    := Factor;
            Expr_Loc :=
              new Binary'
                (Left => Expr_Loc, Operator => Operator, Right => Right);
         end loop;

         return Expr_Loc;
      end Term;

      function Comparison return Expr_Acc is
         Expr_Loc : Expr_Acc := Term;
         Operator : Token;
         Right    : Expr_Acc;
      begin
         while Match (greater)
           or else Match (greater_equal)
           or else Match (less)
           or else Match (less_equal)
         loop
            Operator := Previous;
            Right    := Term;
            Expr_Loc :=
              new Binary'
                (Left => Expr_Loc, Operator => Operator, Right => Right);
         end loop;

         return Expr_Loc;
      end Comparison;

      function Equality return Expr_Acc is
         Expr_Loc : Expr_Acc := Comparison;
         Operator : Token;
         Right    : Expr_Acc;
      begin
         while Match (bang_equal) or else Match (equal_equal) loop
            Operator := Previous;
            Right    := Comparison;
            Expr_Loc :=
              new Binary'
                (Left => Expr_Loc, Operator => Operator, Right => Right);
         end loop;

         return Expr_Loc;
      end Equality;

      function Logical_And return Expr_Acc is
         Expr_Loc : Expr_Acc := Equality;
      begin
         while Match (and_k) loop
            Expr_Loc :=
              new Exprs.Logical'
                (Left => Expr_Loc, Operator => Previous, Right => Equality);
         end loop;

         return Expr_Loc;
      end Logical_And;

      function Logical_Or return Expr_Acc is
         Expr_Loc : Expr_Acc := Logical_And;
      begin
         while Match (or_k) loop
            Expr_Loc :=
              new Exprs.Logical'
                (Left => Expr_Loc, Operator => Previous, Right => Logical_And);
         end loop;

         return Expr_Loc;
      end Logical_Or;

      function Assignment return Expr_Acc is
         Expr_Loc : Expr_Acc := Logical_Or;
      begin
         if Match (equal) then
            declare
               Equals    : Token    := Previous;
               Value_Loc : Expr_Acc := Assignment;
            begin
               if Expr_Loc.all in Exprs.Variable then
                  return new Assign'
                      (Name  => Exprs.Variable (Expr_Loc.all).Name,
                       Value => Value_Loc,
                       Depth => 0);
               elsif Expr_Loc.all in Exprs.Get then
                  return new Exprs.Set'
                      (Object => Exprs.Get (Expr_Loc.all).Object,
                       Name   => Exprs.Get (Expr_Loc.all).Name,
                       Value  => Value_Loc);
               end if;

               Error (Equals.Kind, "Invalid assignment target");
            end;
         end if;

         return Expr_Loc;
      end Assignment;

      function Expression return Expr_Acc is
      begin
         return Assignment;
      end Expression;

      function Declaration return Stmt_Acc;
      function Statement return Stmt_Acc;

      function Block_Statement return Stmts.Stmt_Vectors.Vector is
         Statements : Stmts.Stmt_Vectors.Vector;
      begin
         while not (Check (right_brace)) and then not (Is_At_End) loop
            Statements.Append (Declaration);
         end loop;

         Consume (right_brace, "Expect '}' after block.");

         return Statements;
      end Block_Statement;

      function Expression_Statement return Stmt_Acc is
         value : Expr_Acc := Expression;
      begin
         Consume (semicolon, "Expect ';' after expression.");
         return new Stmts.Expression'(Ex => value);
      end Expression_Statement;

      function For_Statement return Stmt_Acc is
         For_Initializer : Stmt_Acc;
         Condition       : Expr_Acc;
         Increment       : Expr_Acc;
         Loop_Body       : Stmt_Acc;
      begin
         Consume (left_paren, "Expect '(' after 'for'.");
         if Match (semicolon) then
            For_Initializer := null;
         elsif Check (Tokens.var) then
            For_Initializer := Declaration;
         else
            For_Initializer := Expression_Statement;
         end if;

         if not Check (semicolon) then
            Condition := Expression;
         end if;
         Consume (semicolon, "Expect ';' after loop conditions;");

         if not Check (right_paren) then
            Increment := Expression;
         end if;
         Consume (right_paren, "Expect ')' after 'for' clauses;");

         Loop_Body := Statement;

         if Increment /= null then
            declare
               Loop_Body_Vector : Stmt_Vectors.Vector;
            begin
               Loop_Body_Vector.Append (Loop_Body);
               Loop_Body_Vector.Append
               (new Stmts.Expression'(Ex => Increment));
               Loop_Body := new Stmts.Block'(Statements => Loop_Body_Vector);
            end;
         end if;

         if Condition = null then
            Condition :=
              new Exprs.Literal'(Value => Values.Value'(boolean_k, True));
         end if;
         Loop_Body := new Stmts.While_S'(Condition, Loop_Body);

         if For_Initializer /= null then
            declare
               Loop_Body_Vector : Stmt_Vectors.Vector;
            begin
               Loop_Body_Vector.Append (For_Initializer);
               Loop_Body_Vector.Append (Loop_Body);
               Loop_Body := new Stmts.Block'(Statements => Loop_Body_Vector);
            end;
         end if;

         return Loop_Body;
      end For_Statement;

      function If_Statement return Stmt_Acc is
         Condition   : Expr_Acc;
         Then_Branch : Stmt_Acc;
         Else_Branch : Stmt_Acc;
      begin
         Consume (left_paren, "Expect '(' after 'if'.");
         Condition := Expression;
         Consume (right_paren, "Expect ')' after if condition.");

         Then_Branch := Statement;
         if Match (else_k) then
            Else_Branch := Statement;
         end if;

         return new Stmts.If_S'
             (Condition   => Condition,
              Then_Branch => Then_Branch,
              Else_Branch => Else_Branch);
      end If_Statement;

      function Print_Statement return Stmt_Acc is
         value : Expr_Acc := Expression;
      begin
         Consume (semicolon, "Expect ';' after value.");
         return new Stmts.Print'(Ex => value);
      end Print_Statement;

      function Return_Statement return Stmt_Acc is
         Keyword : Token := Previous;
         Value   : Expr_Acc;
      begin
         if not Check (semicolon) then
            Value := Expression;
         end if;

         Consume (semicolon, "Expect ';' after return value");
         return new Stmts.Return_S'(Keyword, Value);
      end Return_Statement;

      function While_Statement return Stmt_Acc is
         Condition : Expr_Acc;
         Loop_Body : Stmt_Acc;
      begin
         Consume (left_paren, "Expect '(' after 'while'.");
         Condition := Expression;
         Consume (right_paren, "Expect ')' after condition.");

         Loop_Body := Statement;

         return new Stmts.While_S'(Condition, Loop_Body);
      end While_Statement;

      function Statement return Stmt_Acc is
      begin
         if Match (for_k) then
            return For_Statement;
         end if;

         if Match (if_k) then
            return If_Statement;
         end if;

         if Match (Tokens.print) then
            return Print_Statement;
         end if;

         if Match (return_k) then
            return Return_Statement;
         end if;

         if Match (while_k) then
            return While_Statement;
         end if;

         if Match (left_brace) then
            return new Stmts.Block'(Statements => Block_Statement);
         end if;

         return Expression_Statement;
      end Statement;

      function Var_Declaration return Stmt_Acc is
         Name        : Token;
         Initializer : Expr_Acc := null;
      begin
         Consume (identifier, "Expect variable name.");
         Name := Previous;
         if Match (equal) then
            Initializer := Expression;
         end if;

         Consume (semicolon, "Expect ';' after variable declaration.");

         return new Stmts.Var'(Name, Initializer);
      end Var_Declaration;

      function Function_Declaration (Kind : String) return Stmt_Acc is
         use type Ada.Containers.Count_Type;
         Name       : Token;
         Parameters : Token_Vectors.Vector;
         Statements : Stmts.Stmt_Vectors.Vector;
      begin
         Consume (identifier, "Expect " & Kind & " name.");
         Name := Previous;

         Consume (left_paren, "Expect '(' after " & Kind & " name.");

         if not Check (right_paren) then
            Parameter_Parser_Loop :
            loop
               if Parameters.Length >=
                 Ada.Containers.Count_Type (Callables.Argument_Number'Last)
               then
                  Error
                    (Peek.Kind,
                     "Can't have more than" &
                     Callables.Argument_Number'Last'Image &
                     "parameters");
               end if;

               Consume (identifier, "Expect parameter name");
               Parameters.Append (Previous);

               exit Parameter_Parser_Loop when not Match (comma);
            end loop Parameter_Parser_Loop;
         end if;
         Consume (right_paren, "Expect ')' after parameters");

         Consume (left_brace, "Expect '{' before " & Kind & " body.");

         Statements := Block_Statement;

         return new Stmts.Function_S'(Name, Parameters, Statements);
      end Function_Declaration;

      function Class_Declaration return Stmt_Acc is
         Name       : Token;
         Methods    : Stmts.Stmt_Vectors.Vector;
         Superclass : Expr_Acc;
      begin
         Consume (identifier, "Expect class name.");
         Name := Previous;

         if Match (less) then
            Consume (identifier, "Expect superclass name.");
            Superclass := new Exprs.Variable'(Previous, 0);
         end if;

         Consume (left_brace, "Expect '{' before class body.");

         while not Check (right_brace) and not Is_At_End loop
            Methods.Append (Function_Declaration ("method"));
         end loop;

         Consume (right_brace, "Expect '}' after class body.");

         return new Stmts.Class'(Name, Superclass, Methods);
      end Class_Declaration;

      function Declaration return Stmt_Acc is
      begin
         if Match (Tokens.class) then
            return Class_Declaration;
         end if;

         if Match (fun) then
            return Function_Declaration ("function");
         end if;

         if Match (Tokens.var) then
            return Var_Declaration;
         end if;

         return Statement;
      exception
         when Parse_Error =>
            Synchronize;
            return null;
      end Declaration;

      procedure Error (Kind : Token_Enum; Msg : String) is
         Current_Token : Token := Previous;
      begin
         if Kind = eof then
            Errors.Put_Error (Current_Token.Line, " at end", Msg);
         else
            Errors.Put_Error
              (Current_Token.Line,
               " at '" & Lexeme_Str.To_String (Current_Token.Lexeme) & "'",
               Msg);
         end if;

         raise Parse_Error;
      end Error;

      procedure Synchronize is
      begin
         Advance;

         while not Is_At_End loop
            if Previous.Kind = semicolon then
               return;
            end if;

            case Peek.Kind is
               when Tokens.class |
                 fun             |
                 Tokens.var      |
                 for_k           |
                 if_k            |
                 while_k         |
                 Tokens.print    |
                 return_k        =>
                  return;
               when others =>
                  null;
            end case;

            Advance;
         end loop;
      end Synchronize;

   begin
      while not Is_At_End loop
         Statements.Append (Declaration);
      end loop;
      return Statements;
   exception
      when Parse_Error =>
         return Statements;
   end Parse;
end Parser;
