with Ada.Containers.Vectors;
with Errors;
with Exprs;  use Exprs;
with Tokens; use Tokens;
with Stmts;  use Stmts;

package Parser is

   function Parse
     (Token_Vector : Token_Vectors.Vector) return Stmts.Stmt_Vectors.Vector;
end Parser;
