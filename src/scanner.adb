with Values;

package body Scanner is

   procedure Initialize_Package is
      use Lexeme_Str;
   begin
      Token_Look_Up.Insert (To_Bounded_String ("and"), and_k);
      Token_Look_Up.Insert (To_Bounded_String ("class"), class);
      Token_Look_Up.Insert (To_Bounded_String ("else"), else_k);
      Token_Look_Up.Insert (To_Bounded_String ("false"), false);
      Token_Look_Up.Insert (To_Bounded_String ("for"), for_k);
      Token_Look_Up.Insert (To_Bounded_String ("fun"), fun);
      Token_Look_Up.Insert (To_Bounded_String ("if"), if_k);
      Token_Look_Up.Insert (To_Bounded_String ("nil"), nil);
      Token_Look_Up.Insert (To_Bounded_String ("or"), or_k);
      Token_Look_Up.Insert (To_Bounded_String ("print"), print);
      Token_Look_Up.Insert (To_Bounded_String ("return"), return_k);
      Token_Look_Up.Insert (To_Bounded_String ("super"), super);
      Token_Look_Up.Insert (To_Bounded_String ("this"), this);
      Token_Look_Up.Insert (To_Bounded_String ("true"), true);
      Token_Look_Up.Insert (To_Bounded_String ("var"), var);
      Token_Look_Up.Insert (To_Bounded_String ("while"), while_k);
   end Initialize_Package;

   function Scan_Tokens (Scn : in out Scanner) return Token_Vectors.Vector is
      New_Token : Token;
   begin
      while not Scn.Is_At_End loop
         Scn.Scan_Token;
      end loop;

      New_Token :=
        Token'
          (Kind       => eof,
           Value_Kind => none,
           Lexeme     => Lexeme_Str.To_Bounded_String (""),
           Line       => Scn.Line);

      Scn.Tokens.Append (New_Token);

      return Scn.Tokens;
   end Scan_Tokens;

   function Is_At_End (Scn : in Scanner) return Boolean is
   begin
      return Scn.Current > Length (Scn.Source);
   end Is_At_End;

   procedure Scan_Token (Scn : in out Scanner) is
      c : Character := Scn.Advance;
   begin
      case c is
         when '(' =>
            Scn.Add_Simple_Token (left_paren);
         when ')' =>
            Scn.Add_Simple_Token (right_paren);
         when '{' =>
            Scn.Add_Simple_Token (left_brace);
         when '}' =>
            Scn.Add_Simple_Token (right_brace);
         when ',' =>
            Scn.Add_Simple_Token (comma);
         when '.' =>
            Scn.Add_Simple_Token (dot);
         when '-' =>
            Scn.Add_Simple_Token (minus);
         when '+' =>
            Scn.Add_Simple_Token (plus);
         when ';' =>
            Scn.Add_Simple_Token (semicolon);
         when '*' =>
            Scn.Add_Simple_Token (star);
         when '!' =>
            Scn.Add_Simple_Token
            (if Scn.Match ('=') then bang_equal else bang);
         when '=' =>
            Scn.Add_Simple_Token
            (if Scn.Match ('=') then equal_equal else equal);
         when '<' =>
            Scn.Add_Simple_Token
            (if Scn.Match ('=') then less_equal else less);
         when '>' =>
            Scn.Add_Simple_Token
            (if Scn.Match ('=') then greater_equal else greater);
         when '/' =>
            if Scn.Match ('/') then
               -- Since we are only parsing line-by-line, this end-of-line check doesn't really do anything.
               while Scn.Peek /= Latin_1.LF and not Scn.Is_At_End loop
                  Scn.Current := Scn.Current + 1;
               end loop;
            else
               Scn.Add_Simple_Token (slash);
            end if;
         when ' ' | Latin_1.HT | Latin_1.CR =>
            null;
         when Latin_1.LF =>
            Scn.Line := Scn.Line + 1;
         when Latin_1.Quotation =>
            Scn.Add_String_Token;
         when others =>
            if Ada.Characters.Handling.Is_Digit (c) then
               Scn.Add_Number_Token;
            elsif Is_Identifier_Character (c) then
               Scn.Add_Identifier_Token;
            else
               Errors.Put_Error (Scn.Line, "Unexpected character: " & c'Image);
            end if;
      end case;

      Scn.Start := Scn.Current;
   end Scan_Token;

   function Advance (Scn : in out Scanner) return Character is
      Current_Character : Character := Element (Scn.Source, Scn.Current);
   begin
      Scn.Current := Scn.Current + 1;
      return Current_Character;
   end Advance;

   function Match
     (Scn      : in out Scanner;
      expected :        Character) return Boolean
   is
   begin
      if Scn.Is_At_End then
         return False;
      end if;

      if Element (Scn.Source, Scn.Current) /= expected then
         return False;
      end if;

      Scn.Current := Scn.Current + 1;
      return True;
   end Match;

   function Peek (Scn : in out Scanner) return Character is
   begin
      if Scn.Is_At_End then
         return Latin_1.NUL;
      end if;

      return Element (Scn.Source, Scn.Current);
   end Peek;

   function Peek_Next (Scn : in out Scanner) return Character is
   begin

      return Element (Scn.Source, Scn.Current + 1);
   exception
      when Ada.Strings.Index_Error =>
         return Latin_1.NUL;
   end Peek_Next;

   procedure Add_Simple_Token
     (Scn        : in out Scanner;
      Token_Kind :        Token_Enum)
   is
      New_Token : Token;
      Literal   : String := Slice (Scn.Source, Scn.Start, Scn.Current - 1);
   begin
      case Token_Kind is
         when string_k | number =>
            null;
         when others =>
            New_Token :=
              Token'
                (Kind       => Token_Kind,
                 Value_Kind => none,
                 Lexeme     => Lexeme_Str.To_Bounded_String (Literal),
                 Line       => Scn.Line);
      end case;

      Scn.Tokens.Append (New_Token);
   end Add_Simple_Token;

   procedure Add_String_Token (Scn : in out Scanner) is
      New_Token : Token;
      Literal   : Lexeme_Str.Bounded_String;

      String : Unbounded_String;
   begin
      while Scn.Peek /= Latin_1.Quotation and not Scn.Is_At_End loop
         if Scn.Peek = Latin_1.LF then
            Scn.Line := Scn.Line + 1;
         end if;
         Scn.Current := Scn.Current + 1;
      end loop;

      if Scn.Is_At_End then
         Errors.Put_Error (Scn.Line, "Unterminated string");
         return;
      end if;

      Scn.Current := Scn.Current + 1;

      String :=
        Unbounded_Slice
          (Source => Scn.Source,
           Low    => Scn.Start + 1,
           High   => Scn.Current - 2);

      Literal :=
        Lexeme_Str.To_Bounded_String
          (Slice (Scn.Source, Scn.Start, Scn.Current - 1));

      New_Token :=
        Token'
          (Value_Kind => string_k,
           Kind       => string_k,
           Lexeme     => Literal,
           Line       => Scn.Line,
           String     => String);

      Scn.Tokens.Append (New_Token);
   end Add_String_Token;

   procedure Add_Number_Token (Scn : in out Scanner) is
      use Ada.Characters.Handling;
      New_Token : Token;
      Literal   : Lexeme_Str.Bounded_String;
      Value     : Values.LoxNumber;
   begin
      while Is_Digit (Scn.Peek) loop
         Scn.Current := Scn.Current + 1;
      end loop;

      if Scn.Peek = '.' and Is_Digit (Scn.Peek_Next) then
         Scn.Current := Scn.Current + 1;

         while Is_Digit (Scn.Peek) loop
            Scn.Current := Scn.Current + 1;
         end loop;
      end if;

      Literal :=
        Lexeme_Str.To_Bounded_String
          (Slice (Scn.Source, Scn.Start, Scn.Current - 1));

      Value := Values.LoxNumber'Value (Lexeme_Str.To_String (Literal));

      New_Token :=
        Token'
          (Value_Kind => number,
           Kind       => number,
           Lexeme     => Literal,
           Line       => Scn.Line,
           Number     => Value);

      Scn.Tokens.Append (New_Token);
   end Add_Number_Token;

   procedure Add_Identifier_Token (Scn : in out Scanner) is
      Literal : Lexeme_Str.Bounded_String;
      Kind    : Token_Enum := identifier;
   begin
      while Is_Identifier_Character (Scn.Peek) loop
         Scn.Current := Scn.Current + 1;
      end loop;

      Literal :=
        Lexeme_Str.To_Bounded_String
          (Slice (Scn.Source, Scn.Start, Scn.Current - 1));

      -- Work on reserved words
      if Token_Look_Up.Contains (Literal) then
         Kind := Token_Look_Up.Element (Literal);
         Scn.Add_Simple_Token (Kind);
      else
         Scn.Add_Simple_Token (identifier);
      end if;

   end Add_Identifier_Token;

   function Is_Identifier_Character (c : Character) return Boolean is
   begin
      return c = '_' or Ada.Characters.Handling.Is_Alphanumeric (c);
   end Is_Identifier_Character;

begin
   Initialize_Package;
end Scanner;
