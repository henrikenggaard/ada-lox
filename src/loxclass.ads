limited with Interpreter;
with Callables;
with Environments;
with Tokens;
with Values;

package LoxClass is
   type LoxClass is new Callables.Callable with record
      Name       : Tokens.Token;
      Superclass : access LoxClass;
      Methods    : access Environments.Environment;
   end record;

   function Construct
     (Name       : Tokens.Token;
      Superclass : access LoxClass) return access LoxClass;

   function Arity (Self : in out LoxClass) return Callables.Argument_Number;

   function Call
     (Self      : in out LoxClass;
      I         : in out Interpreter.Interpreter'Class;
      Arguments :    Callables.Argument_Vectors.Vector) return Values.Value;

   function Image (Self : in out LoxClass) return String;
end LoxClass;
