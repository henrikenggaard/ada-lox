with Environments;
with Interpreter;
with LoxInstance;

package body LoxFunctions is
   function Arity
     (Self : in out LoxFunction) return Callables.Argument_Number
   is
      use type Tokens.Token_Vectors.Vector;
   begin
      return Callables.Argument_Number (Self.Parameters.Length);
   end Arity;

   function Bind
     (Original : LoxFunction;
      Instance : access LoxInstance.Instance) return access LoxFunction
   is
      Env : access Environments.Environment :=
        Environments.Construct (Original.Closure);
      Instance_Value : Values.Value :=
        Values.Value'(Values.instance, Instance);
   begin
      Env.Define
      (Tokens.Lexeme_Str.To_Bounded_String ("this"), Instance_Value);
      return new LoxFunction'
          (Name       => Original.Name,
           Parameters => Original.Parameters,
           Statements => Original.Statements,
           Closure    => Env,
           Kind       => Original.Kind);
   end Bind;

   function Call
     (Self      : in out LoxFunction;
      I         : in out Interpreter.Interpreter'Class;
      Arguments :        Callables.Argument_Vectors.Vector) return Values.Value
   is
      Call_Env : access Environments.Environment :=
        Environments.Construct (Self.Closure);
   begin
      for J in Self.Parameters.First_Index .. Self.Parameters.Last_Index loop
         Call_Env.Define
         (Self.Parameters.Element (J)
            .Lexeme, Arguments.Element
          (Callables.Argument_Number (J)));
      end loop;

      declare
      begin
         I.Execute_Block (Self.Statements, Call_Env);
      exception
         when Err : Interpreter.Return_Control_Flow =>
            return Interpreter.Get_Return_Value;
      end;

      if Self.Kind = Initializer then
         return Call_Env.all.Get
           (Tokens.Lexeme_Str.To_Bounded_String ("this"));
      end if;

      return Values.Value'(Kind => Values.nil);
   end Call;

   function Image (Self : in out LoxFunction) return String is
      Name : String := Tokens.Lexeme_Str.To_String (Self.Name.Lexeme);
   begin
      return "<fn " & Name & ">";
   end Image;
end LoxFunctions;
