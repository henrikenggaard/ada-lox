with LoxClass;
with Environments;
with Tokens;
with Values;

package LoxInstance is
   type Instance is tagged private;

   function Construct (Class : LoxClass.LoxClass) return access Instance;

   function Image (Self : in out Instance) return String;

   function Get
     (Self : in out Instance;
      Key  :        Tokens.Lexeme_Str.Bounded_String) return Values.Value;

   procedure Set
     (Self : in out Instance;
      Key  :        Tokens.Lexeme_Str.Bounded_String;
      V    :        Values.Value);

private
   type Instance is tagged record
      Class      : LoxClass.LoxClass;
      Properties : access Environments.Environment;
      Methods    : access Environments.Environment;
   end record;

end LoxInstance;
