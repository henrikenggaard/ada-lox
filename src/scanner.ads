with Ada.Characters.Handling;
with Ada.Characters.Latin_1;
with Ada.Containers.Hashed_Maps;
with Ada.Strings.Bounded.Hash;
with Ada.Strings.Unbounded; use Ada.Strings.Unbounded;
with Errors;
with Tokens;                use Tokens;

package Scanner is
   pragma Elaborate_Body;

   package Latin_1 renames Ada.Characters.Latin_1;

   type Scanner is tagged record
      Line    : Tokens.Line_Number := 1;
      Start   : Positive           := 1;
      Current : Natural            := 1;
      Source  : Unbounded_String;
      Tokens  : Token_Vectors.Vector;
   end record;

   function Scan_Tokens (Scn : in out Scanner) return Token_Vectors.Vector;

   function Is_At_End (Scn : in Scanner) return Boolean;

private
   procedure Initialize_Package;

   function Hash is new Ada.Strings.Bounded.Hash (Lexeme_Str);

   package Token_Map is new Ada.Containers.Hashed_Maps
     (Key_Type        => Lexeme_Str.Bounded_String,
      Element_Type    => Token_Enum,
      Hash            => Hash,
      Equivalent_Keys => Lexeme_Str."=");

   Token_Look_Up : Token_Map.Map;

   procedure Scan_Token (Scn : in out Scanner);

   function Advance (Scn : in out Scanner) return Character;

   function Match (Scn : in out Scanner; expected : Character) return Boolean;

   function Peek (Scn : in out Scanner) return Character;

   function Peek_Next (Scn : in out Scanner) return Character;

   procedure Add_Simple_Token (Scn : in out Scanner; Token_Kind : Token_Enum);

   procedure Add_String_Token (Scn : in out Scanner);

   procedure Add_Number_Token (Scn : in out Scanner);

   procedure Add_Identifier_Token (Scn : in out Scanner);

   function Is_Identifier_Character (c : Character) return Boolean;
end Scanner;
