with Ada.Containers.Vectors;
with Ada.Strings.Bounded;
with Ada.Strings.Unbounded; use Ada.Strings.Unbounded;
with Values;

package Tokens is
   type Token_Enum is
     (
   -- Single-character tokens
   left_paren,
      right_paren,
      left_brace,
      right_brace,
      comma,
      dot,
      minus,
      plus,
      semicolon,
      slash,
      star,

   -- One or two character tokens
      bang,
      bang_equal,
      equal,
      equal_equal,
      greater,
      greater_equal,
      less,
      less_equal,

   -- Literals
      identifier,
      string_k,
      number,

   -- Keywords
      and_k,
      class,
      else_k,
      false,
      fun,
      for_k,
      if_k,
      nil,
      or_k,
      print,
      return_k,
      super,
      this,
      true,
      var,
      while_k,
      eof);

   type Token_Value is (identifier, string_k, number, none);

   package Lexeme_Str is new Ada.Strings.Bounded.Generic_Bounded_Length
     (Max => 100);

   type Line_Number is new Positive;

   type Token (Value_Kind : Token_Value := none) is record
      Kind   : Token_Enum;
      Lexeme : Lexeme_Str.Bounded_String;
      Line   : Line_Number;
      case Value_Kind is
         when string_k =>
            String : Unbounded_String;
         when number =>
            Number : Values.LoxNumber;
         when others =>
            null;
      end case;
   end record;

   package Token_Vectors is new Ada.Containers.Vectors
     (Index_Type   => Natural,
      Element_Type => Token);

   function Image (T : Token) return String;

private
end Tokens;
