package body Errors is

   function Had_Error return Boolean is
   begin
      return prv_Had_Error;
   end Had_Error;

   procedure Reset_Error is
   begin
      prv_Had_Error     := False;
      Had_Runtime_Error := False;
   end Reset_Error;

   function Error_Msg
     (Line_No : Tokens.Line_Number;
      Msg     : String;
      Where   : String := "") return String
   is
   begin
      prv_Had_Error := True;
      return "[Line " & Line_No'Image & "] Error" & Where & ": " & Msg;
   end Error_Msg;

   procedure Put_Error
     (Line_No : Tokens.Line_Number;
      Msg     : String;
      Where   : String := "")
   is
   begin
      Ada.Text_IO.Put_Line (Error_Msg (Line_No, Msg, Where));
   end Put_Error;

end Errors;
