with Ada.Calendar;
with Ada.Containers;
with Ada.Exceptions;
with Ada.Strings.Unbounded;
with Ada.Text_IO;
with Callables;
with Errors;
with LoxClass;
with LoxFunctions;
with LoxInstance;
with Tokens; use Tokens;
with Values; use Values;

package body Interpreter is

   procedure Raise_Error (T : Tokens.Token; S : String) is
   begin
      raise Runtime_Error with Errors.Error_Msg (T.Line, S);
   end Raise_Error;

   procedure Raise_Error
     (T : Tokens.Token;
      X : Ada.Exceptions.Exception_Occurrence)
   is
      use Ada.Exceptions;
   begin
      Raise_Error (T, Exception_Message (X));
   end Raise_Error;

   overriding procedure Visit_Assign
     (Self : in out Interpreter;
      Obj  : in out Exprs.Assign'Class)
   is
   begin
      Obj.Value.Acceptor (Self'Access);
      Own.Assign_At (Obj.Name.Lexeme, Return_Value, Obj.Depth);
   exception
      when E : Environments.Not_Declared_Error =>
         Raise_Error (Obj.Name, E);
   end Visit_Assign;

   overriding procedure Visit_Binary
     (Self : in out Interpreter;
      Obj  : in out Exprs.Binary'Class)
   is
      use Ada.Strings.Unbounded;
      use Exprs;
      Right_Value : Values.Value;
   begin
      null;
      Obj.Right.Acceptor (Self'Access);
      Right_Value := Return_Value;
      Obj.Left.Acceptor (Self'Access);
      case Obj.Operator.Kind is
         when minus =>
            Return_Value := Return_Value - Right_Value;
         when star =>
            Return_Value := Return_Value * Right_Value;
         when slash =>
            Return_Value := Return_Value / Right_Value;
         when plus =>
            Return_Value := Return_Value + Right_Value;
         when greater =>
            Return_Value := Return_Value > Right_Value;
         when greater_equal =>
            Return_Value := Return_Value >= Right_Value;
         when less =>
            Return_Value := Return_Value < Right_Value;
         when less_equal =>
            Return_Value := Return_Value <= Right_Value;
         when bang_equal =>
            Return_Value := Return_Value /= Right_Value;
         when equal_equal =>
            Return_Value := Return_Value = Right_Value;
         when others =>
            null;
      end case;
   exception
      when E : Value_Error =>
         Raise_Error (Obj.Operator, E);
   end Visit_Binary;

   overriding procedure Visit_Call
     (Self : in out Interpreter;
      Obj  : in out Exprs.Call'Class)
   is
      use type Ada.Containers.Count_Type;
      Evaluated_Arguments : Callables.Argument_Vectors.Vector;
      Callee              : Values.Value;
   begin
      Obj.Callee.Acceptor (Self'Access);
      Callee := Return_Value;

      for Arg of Obj.Arguments loop
         Arg.Acceptor (Self'Access);
         Evaluated_Arguments.Append (Return_Value);
      end loop;

      if Callee.Kind /= Values.callable then
         Raise_Error (Obj.paren, "Only functions and classes can be called.");
      end if;

      if Evaluated_Arguments.Length /=
        Ada.Containers.Count_Type (Callee.Callee.Arity)
      then
         Raise_Error
           (Obj.paren,
            "Expected" &
            Callee.Callee.Arity'Image &
            " arguments, got" &
            Evaluated_Arguments.Length'Image &
            ".");
      end if;

      Return_Value := Callee.Callee.Call (Self, Evaluated_Arguments);
   end Visit_Call;

   overriding procedure Visit_Get
     (Self : in out Interpreter;
      Obj  : in out Exprs.Get'Class)
   is
   begin
      Obj.Object.Acceptor (Self'Access);
      if Return_Value.Kind = Values.instance then
         Return_Value := Return_Value.I.Get (Key => Obj.Name.Lexeme);
         return;
      end if;

      Raise_Error (Obj.Name, "Only instances have properties.");
   exception
      when E : Environments.Not_Declared_Error =>
         Raise_Error (Obj.Name, E);
   end Visit_Get;

   overriding procedure Visit_Grouping
     (Self : in out Interpreter;
      Obj  : in out Exprs.Grouping'Class)
   is
   begin
      Obj.Expression.Acceptor (Self'Access);
   end Visit_Grouping;

   overriding procedure Visit_Literal
     (Self : in out Interpreter;
      Obj  : in out Exprs.Literal'Class)
   is
   begin
      Return_Value := Obj.Value;
   end Visit_Literal;

   overriding procedure Visit_Logical
     (Self : in out Interpreter;
      Obj  : in out Exprs.Logical'Class)
   is
   begin
      Obj.Left.Acceptor (Self'Access);

      if Obj.Operator.Kind = Tokens.or_k then
         if Values.To_Ada_Bool (Return_Value) then
            return;
         end if;
      else
         if not Values.To_Ada_Bool (Return_Value) then
            return;
         end if;
      end if;

      Obj.Right.Acceptor (Self'Access);
   end Visit_Logical;

   overriding procedure Visit_Set
     (Self : in out Interpreter;
      Obj  : in out Exprs.Set'Class)
   is
      Object : Values.Value;
   begin
      Obj.Object.Acceptor (Self'Access);

      Object := Return_Value;

      if Object.Kind /= Values.instance then
         Raise_Error (Obj.Name, "Only instances have fields.");
      end if;

      Obj.Value.Acceptor (Self'Access);

      Object.I.Set (Obj.Name.Lexeme, Return_Value);
   end Visit_Set;

   overriding procedure Visit_Super
     (Self : in out Interpreter;
      Obj  : in out Exprs.Super'Class)
   is
      this : Lexeme_Str.Bounded_String :=
        Lexeme_Str.To_Bounded_String ("this");
      super : Lexeme_Str.Bounded_String :=
        Lexeme_Str.To_Bounded_String ("super");

      Superclass : access LoxClass.LoxClass;
   begin
      declare
         V : Value := Own.Get_At (super, Obj.Depth);
      begin
         Superclass := LoxClass.LoxClass (V.Callee.all)'Access;
      end;

      declare
         Instance_Value : Value := Own.Get_At (this, Obj.Depth + 1);
         Method_Value   : Value := Superclass.Methods.Get (Obj.Method.Lexeme);
         use LoxFunctions;
      begin
         Method_Value.Callee :=
           Bind
             (LoxFunction (Method_Value.Callee.all),
              LoxInstance.Instance (Instance_Value.Callee.all)'Access);
         Return_Value := Method_Value;
      end;
   exception
      when E : Environments.Not_Declared_Error =>
         Raise_Error (Obj.Keyword, E);
   end Visit_Super;

   overriding procedure Visit_This
     (Self : in out Interpreter;
      Obj  : in out Exprs.This'Class)
   is
   begin
      Return_Value := Own.Get (Lexeme_Str.To_Bounded_String ("this"));
   end Visit_This;

   overriding procedure Visit_Unary
     (Self : in out Interpreter;
      Obj  : in out Exprs.Unary'Class)
   is
      use Exprs;
   begin
      Obj.Right.Acceptor (Self'Access);
      case Obj.Operator.Kind is
         when minus =>
            Return_Value := -Return_Value;
         when bang =>
            Return_Value := not To_Bool (Return_Value);
         when others =>
            null;
      end case;
   exception
      when E : Value_Error =>
         Raise_Error (Obj.Operator, E);
   end Visit_Unary;

   overriding procedure Visit_Variable
     (Self : in out Interpreter;
      Obj  : in out Variable'Class)
   is
   begin
      Return_Value := Own.Get_At (Obj.Name.Lexeme, Obj.Depth);
   exception
      when E : Environments.Not_Declared_Error =>
         Raise_Error (Obj.Name, E);
   end Visit_Variable;

   overriding procedure Visit_Block
     (Self : in out Interpreter;
      Obj  : in out Block'Class)
   is
      New_Env : access Environments.Environment :=
        Environments.Construct (Own);
   begin
      Execute_Block (Self, Obj.Statements, New_Env);
   end Visit_Block;

   overriding procedure Visit_Class
     (Self : in out Interpreter;
      Obj  : in out Stmts.Class'Class)
   is
      Method_Instance  : access LoxFunctions.LoxFunction;
      Method_Value     : Value;
      Class_Instance   : access LoxClass.LoxClass;
      Class_Value      : Value;
      Superclass       : access LoxClass.LoxClass;
      Superclass_Value : Value;

      Previous_Env : access Environments.Environment := Own;
   begin
      if Obj.Superclass /= null then
         Obj.Superclass.Acceptor (Self'Access);

         if not
           (Return_Value.Kind = callable
            and then Return_Value.Callee.all in LoxClass.LoxClass)
         then
            Raise_Error (Obj.Name, "Superclass must be a class.");
         end if;

         Superclass := LoxClass.LoxClass (Return_Value.Callee.all)'Access;
         Superclass_Value := Return_Value;
      end if;

      Own.Define (Obj.Name.Lexeme, Value'(Kind => nil));

      if Obj.Superclass /= null then
         Own := Environments.Construct (Own);
         Own.Define (Lexeme_Str.To_Bounded_String ("super"), Superclass_Value);
      end if;

      Class_Instance := LoxClass.Construct (Obj.Name, Superclass);

      for Raw_Stmt of Obj.Methods loop
         declare
            use LoxFunctions;
            use Tokens.Lexeme_Str;

            Method_Stmt : Stmts.Function_S := Stmts.Function_S (Raw_Stmt.all);
            Kind        : Function_Kind    := Normal;
         begin
            if Method_Stmt.Name.Lexeme = "init" then
               Kind := Initializer;
            end if;

            Method_Instance :=
              new LoxFunctions.LoxFunction'
                (Name       => Method_Stmt.Name,
                 Parameters => Method_Stmt.Parameters,
                 Statements => Method_Stmt.Statements,
                 Closure    => Own,
                 Kind       => Kind);

            Method_Value :=
              Value'(Kind => callable, Callee => Method_Instance);

            Class_Instance.Methods.Define
            (Method_Stmt.Name.Lexeme, Method_Value);
         end;
      end loop;

      if Obj.Superclass /= null then
         Own := Previous_Env;
      end if;

      Class_Value := Value'(Kind => callable, Callee => Class_Instance);
      Own.Assign (Obj.Name.Lexeme, Class_Value);
   end Visit_Class;

   overriding procedure Visit_Expression
     (Self : in out Interpreter;
      Obj  : in out Expression'Class)
   is
   begin
      Obj.Ex.Acceptor (Self'Access);
   end Visit_Expression;

   overriding procedure Visit_Function_S
     (Self : in out Interpreter;
      Obj  : in out Stmts.Function_S'Class)
   is
      Function_Instance : access LoxFunctions.LoxFunction;
      Function_Value    : Value;
   begin
      Function_Instance :=
        new LoxFunctions.LoxFunction'
          (Name       => Obj.Name,
           Parameters => Obj.Parameters,
           Statements => Obj.Statements,
           Closure    => Own,
           Kind       => LoxFunctions.Normal);

      Function_Value := Value'(Kind => callable, Callee => Function_Instance);
      Own.Define (Obj.Name.Lexeme, Function_Value);
   end Visit_Function_S;

   overriding procedure Visit_If_S
     (Self : in out Interpreter;
      Obj  : in out If_S'Class)
   is
   begin
      Obj.Condition.Acceptor (Self'Access);
      if Values.To_Ada_Bool (Return_Value) then
         Obj.Then_Branch.Acceptor (Self'Access);
      elsif Obj.Else_Branch /= null then
         Obj.Else_Branch.Acceptor (Self'Access);
      end if;
   end Visit_If_S;

   overriding procedure Visit_Print
     (Self : in out Interpreter;
      Obj  : in out Stmts.Print'Class)
   is
   begin
      Obj.Ex.Acceptor (Self'Access);
      Ada.Text_IO.Put_Line (To_String);
   end Visit_Print;

   overriding procedure Visit_Var
     (Self : in out Interpreter;
      Obj  : in out Stmts.Var'Class)
   is
      Initializer : Values.Value;
   begin
      if Obj.Initializer /= null then
         Obj.Initializer.Acceptor (Self'Access);
         Initializer := Return_Value;
      else
         Initializer := Values.Value'(Kind => Values.nil);
      end if;
      Own.Define (Obj.Name.Lexeme, Initializer);
   end Visit_Var;

   overriding procedure Visit_Return_S
     (Self : in out Interpreter;
      Obj  : in out Stmts.Return_S'Class)
   is
   begin
      Return_Value := Value'(Kind => nil);
      if Obj.Value /= null then
         Obj.Value.Acceptor (Self'Access);
      end if;

      raise Return_Control_Flow;
   end Visit_Return_S;

   overriding procedure Visit_While_S
     (Self : in out Interpreter;
      Obj  : in out Stmts.While_S'Class)
   is
   begin
      Obj.Condition.Acceptor (Self'Access);
      while Values.To_Ada_Bool (Return_Value) loop
         Obj.Loop_Body.Acceptor (Self'Access);
         Obj.Condition.Acceptor (Self'Access);
      end loop;
   end Visit_While_S;

   function To_String return String is
   begin
      return Values.To_String (Return_Value);
   end To_String;

   procedure Excute (Self : in out Interpreter; Statement : Stmts.Stmt_Acc) is
   begin
      Statement.Acceptor (Self'Access);
   end Excute;

   procedure Execute_Block
     (Self       : in out Interpreter;
      Statements :        Stmts.Stmt_Vectors.Vector;
      Env        :        access Environments.Environment)
   is
      Previous_Env : access Environments.Environment := Own;
   begin
      Own := Env;

      for S of Statements loop
         Excute (Self, S);
      end loop;

      Own := Previous_Env;
   exception
      when Return_Control_Flow =>
         -- Ensure that we always restore the previous environment when returning from function calls
         Own := Previous_Env;
         raise Return_Control_Flow;
   end Execute_Block;

   procedure Interpret
     (Self       : in out Interpreter;
      Statements :        Stmts.Stmt_Vectors.Vector)
   is
   begin
      for Statement of Statements loop
         Excute (Self, Statement);
      end loop;
   exception
      when Err : Runtime_Error =>
         Ada.Text_IO.Put_Line (Ada.Exceptions.Exception_Message (Err));
         Errors.Had_Runtime_Error := True;
         return;
   end Interpret;

   -- `clock` function

   type Clock_Callable is new Callables.Callable with null record;

   overriding function Call
     (Self      : in out Clock_Callable;
      I         : in out Interpreter'Class;
      Arguments :    Callables.Argument_Vectors.Vector) return Values.Value;

   overriding function Image (Self : in out Clock_Callable) return String;

   overriding function Arity
     (Self : in out Clock_Callable) return Callables.Argument_Number
   is
   begin
      return 0;
   end Arity;

   overriding function Call
     (Self      : in out Clock_Callable;
      I         : in out Interpreter'Class;
      Arguments :        Callables.Argument_Vectors.Vector) return Values.Value
   is
      use type Ada.Calendar.Time;
      Epoch : constant Ada.Calendar.Time :=
        Ada.Calendar.Time_Of (1970, 1, 1, 0.0);
      Seconds : Values.LoxNumber :=
        Values.LoxNumber (Ada.Calendar.Clock - Epoch);
   begin
      return Values.Value'(Kind => number, Number => Seconds);
   end Call;

   overriding function Image (Self : in out Clock_Callable) return String is
   begin
      return "<fn clock>";
   end Image;

   Clock_Instance : aliased Clock_Callable;
   The_Clock      : Values.Value :=
     (Kind => callable, Callee => Clock_Instance'Access);

   -- `env` function

   type Env_Callable is new Callables.Callable with null record;

   overriding function Call
     (Self      : in out Env_Callable;
      I         : in out Interpreter'Class;
      Arguments :    Callables.Argument_Vectors.Vector) return Values.Value;

   overriding function Image (Self : in out Env_Callable) return String;

   overriding function Arity
     (Self : in out Env_Callable) return Callables.Argument_Number
   is
   begin
      return 0;
   end Arity;

   overriding function Call
     (Self      : in out Env_Callable;
      I         : in out Interpreter'Class;
      Arguments :        Callables.Argument_Vectors.Vector) return Values.Value
   is
   begin
      Own.Print;
      return Values.Value'(Kind => nil);
   end Call;

   overriding function Image (Self : in out Env_Callable) return String is
   begin
      return "<fn __env>";
   end Image;

   Env_Function_Instance : aliased Env_Callable;
   The_Env_Function      : Values.Value :=
     (Kind => callable, Callee => Env_Function_Instance'Access);

   function Get_Return_Value return Values.Value is
   begin
      return Return_Value;
   end Get_Return_Value;

begin
   Globals := new Environments.Environment;
   Globals.Define (Lexeme_Str.To_Bounded_String ("clock"), The_Clock);
   Globals.Define (Lexeme_Str.To_Bounded_String ("__env"), The_Env_Function);

   Own := Globals;
end Interpreter;
