with Callables;
with LoxInstance;

package body Values is
   procedure Check_Number_Operands (A, B : Value);

   function "not" (V : Value) return Value is
      new_state : Boolean := not V.Bool_V;
   begin
      return Value'(boolean_k, new_state);
   end "not";

   function "=" (A, B : Value) return Value is
      state : Boolean := False;
   begin
      if A.Kind = B.Kind then
         case A.Kind is
            when boolean_k =>
               state := A.Bool_V = B.Bool_V;
            when callable | instance =>
               -- Figure out how to compare callables
               state := False;
            when nil =>
               state := True;
            when number =>
               state := A.Number = B.Number;
            when string_k =>
               state := A.String = B.String;
         end case;
      end if;

      return Value'(boolean_k, state);
   end "=";

   function "/=" (A, B : Value) return Value is
      temp : Value := A = B;
   begin
      temp.Bool_V := not temp.Bool_V;
      return temp;
   end "/=";

   function "<" (A, B : Value) return Value is
   begin
      return Value'(boolean_k, A.Number < B.Number);
   end "<";

   function "<=" (A, B : Value) return Value is
   begin
      return Value'(boolean_k, A.Number <= B.Number);
   end "<=";

   function ">" (A, B : Value) return Value is
   begin
      return Value'(boolean_k, A.Number > B.Number);
   end ">";

   function ">=" (A, B : Value) return Value is
   begin
      return Value'(boolean_k, A.Number >= B.Number);
   end ">=";

   function "+" (A, B : Value) return Value is
   begin
      if A.Kind = number and then B.Kind = number then
         return Value'(number, A.Number + B.Number);
      end if;

      if A.Kind = string_k and then B.Kind = string_k then
         return Value'(string_k, A.String & B.String);
      end if;

      raise Value_Error with "Operands must be two numbers or two strings.";
   end "+";

   function "-" (V : Value) return Value is
   begin
      if V.Kind = number then
         return Value'(number, -V.Number);
      end if;

      raise Value_Error with "Operand must be a number.";
   end "-";

   function "-" (A, B : Value) return Value is
   begin
      Check_Number_Operands (A, B);
      return Value'(number, A.Number - B.Number);
   end "-";

   function "*" (A, B : Value) return Value is
   begin
      Check_Number_Operands (A, B);
      return Value'(number, A.Number * B.Number);
   end "*";

   function "/" (A, B : Value) return Value is
   begin
      Check_Number_Operands (A, B);
      if B.Number = 0.0 then
         raise Value_Error with "Divide by zero.";
      end if;

      return Value'(number, A.Number / B.Number);
   end "/";

   function To_Bool (V : Value) return Value is
   begin
      case V.Kind is
         when nil =>
            return Value'(boolean_k, False);
         when boolean_k =>
            return V;
         when others =>
            return Value'(boolean_k, True);
      end case;
   end To_Bool;

   function To_Ada_Bool (V : Value) return Boolean is
      Bool_V : Value := To_Bool (V);
   begin
      return Bool_V.Bool_V;
   end To_Ada_Bool;

   function To_String (V : Value) return String is
   begin
      case V.Kind is
         when boolean_k =>
            return V.Bool_V'Image;
         when callable =>
            return V.Callee.Image;
         when instance =>
            return V.I.Image;
         when nil =>
            return "nil";
         when number =>
            declare
               trunc : Values.LoxNumber :=
                 Values.LoxNumber'Truncation (V.Number);
            begin
               if trunc = V.Number then
                  declare
                     int : Integer := Integer (trunc);
                  begin
                     return int'Image;
                  end;
               end if;
               return V.Number'Image;
            end;
         when string_k =>
            return Ada.Strings.Unbounded.To_String (V.String);
      end case;
   end To_String;

   procedure Check_Number_Operands (A, B : Value) is
   begin
      if A.Kind = number and then B.Kind = number then
         return;
      end if;

      raise Value_Error with "Operands must be numbers.";
   end Check_Number_Operands;
end Values;
