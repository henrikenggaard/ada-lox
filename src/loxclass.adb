with LoxInstance;
with LoxFunctions;

package body LoxClass is
   function Construct
     (Name       : Tokens.Token;
      Superclass : access LoxClass) return access LoxClass
   is
   begin
      return new LoxClass'
          (Name       => Name,
           Superclass => Superclass,
           Methods    => new Environments.Environment);
   end Construct;

   function Arity (Self : in out LoxClass) return Callables.Argument_Number is
      use type Tokens.Token_Vectors.Vector;
      use Tokens.Lexeme_Str;
      Init_String : Bounded_String := To_Bounded_String ("init");
   begin
      if not Self.Methods.Contains (Init_String) then
         return Callables.Argument_Number (0);
      end if;

      declare
         Init_Method_Value : Values.Value := Self.Methods.Get (Init_String);
      begin
         return Init_Method_Value.Callee.Arity;
      end;
   end Arity;

   function Call
     (Self      : in out LoxClass;
      I         : in out Interpreter.Interpreter'Class;
      Arguments :        Callables.Argument_Vectors.Vector) return Values.Value
   is
      Instance : access LoxInstance.Instance := LoxInstance.Construct (Self);
      Instance_Value : Values.Value :=
        Values.Value'(Kind => Values.instance, I => Instance);
      use Tokens.Lexeme_Str;
      Init_String : Bounded_String := To_Bounded_String ("init");
   begin
      if not Self.Methods.Contains (Init_String) then
         return Instance_Value;
      end if;

      declare
         use LoxFunctions;
         Init_Method_Value : Values.Value := Self.Methods.Get (Init_String);
         Init_Method       : access LoxFunctions.LoxFunction;
         Discarded         : Values.Value;
      begin
         Init_Method :=
           Bind (LoxFunction (Init_Method_Value.Callee.all), Instance);
         Discarded := Init_Method.Call (I, Arguments);
      end;

      return Instance_Value;
   end Call;

   function Image (Self : in out LoxClass) return String is
   begin
      return "<class " & Tokens.Lexeme_Str.To_String (Self.Name.Lexeme) & ">";
   end Image;
end LoxClass;
