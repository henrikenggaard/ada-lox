with Values; use Values;
with Tokens;
with Ada.Characters.Latin_9;

package body AST_Printer is
   overriding procedure Visit_Assign
     (Self : in out Printer;
      Obj  : in out Exprs.Assign'Class)
   is
   begin
      Append (AST_Output, "(assign ");
      Append (AST_Output, Tokens.Lexeme_Str.To_String (Obj.Name.Lexeme));
      Append (AST_Output, " ");
      Obj.Value.Acceptor (Self'Access);
      Append (AST_Output, ")");
   end Visit_Assign;

   overriding procedure Visit_Binary
     (Self : in out Printer;
      Obj  : in out Exprs.Binary'Class)
   is
   begin
      Append (AST_Output, "(");
      Append (AST_Output, Obj.Operator.Kind'Image);
      Append (AST_Output, " ");
      Obj.Left.Acceptor (Self'Access);
      Append (AST_Output, " ");
      Obj.Right.Acceptor (Self'Access);
      Append (AST_Output, ")");
   end Visit_Binary;

   overriding procedure Visit_Call
     (Self : in out Printer;
      Obj  : in out Exprs.Call'Class)
   is
   begin
      Append (AST_Output, "(");
      Obj.Callee.Acceptor (Self'Access);
      for Arg of Obj.Arguments loop
         Append (AST_Output, " ");
         Arg.Acceptor (Self'Access);
      end loop;
      Append (AST_Output, ")");
   end Visit_Call;

   overriding procedure Visit_Get
     (Self : in out Printer;
      Obj  : in out Exprs.Get'Class)
   is
   begin
      Append (AST_Output, "[get ");
      Obj.Object.Acceptor (Self'Access);
      Append (AST_Output, ".");
      Append (AST_Output, Tokens.Lexeme_Str.To_String (Obj.Name.Lexeme));

      Append (AST_Output, "]");
   end Visit_Get;

   overriding procedure Visit_Grouping
     (Self : in out Printer;
      Obj  : in out Exprs.Grouping'Class)
   is
   begin
      Obj.Expression.Acceptor (Self'Access);
   end Visit_Grouping;

   overriding procedure Visit_Literal
     (Self : in out Printer;
      Obj  : in out Exprs.Literal'Class)
   is
   begin
      Append (AST_Output, To_Unbounded_String (Values.To_String (Obj.Value)));
   end Visit_Literal;

   overriding procedure Visit_Logical
     (Self : in out Printer;
      Obj  : in out Exprs.Logical'Class)
   is
   begin
      Append (AST_Output, "(");
      Append (AST_Output, Obj.Operator.Kind'Image);
      Append (AST_Output, " ");
      Obj.Left.Acceptor (Self'Access);
      Append (AST_Output, " ");
      Obj.Right.Acceptor (Self'Access);
      Append (AST_Output, ")");
   end Visit_Logical;

   overriding procedure Visit_Set
     (Self : in out Printer;
      Obj  : in out Exprs.Set'Class)
   is
   begin
      Append (AST_Output, "(set ");
      Obj.Object.Acceptor (Self'Access);
      Append (AST_Output, ".");
      Append (AST_Output, Tokens.Lexeme_Str.To_String (Obj.Name.Lexeme));
      Append (AST_Output, " ");
      Obj.Value.Acceptor (Self'Access);
      Append (AST_Output, ")");
   end Visit_Set;

   overriding procedure Visit_Super
     (Self : in out Printer;
      Obj  : in out Exprs.Super'Class)
   is
   begin
      Append (AST_Output, "(super.");
      Append (AST_Output, Tokens.Lexeme_Str.To_String (Obj.Method.Lexeme));
      Append (AST_Output, ")");
   end Visit_Super;

   overriding procedure Visit_This
     (Self : in out Printer;
      Obj  : in out Exprs.This'Class)
   is
   begin
      Append (AST_Output, "This");
   end Visit_This;

   overriding procedure Visit_Unary
     (Self : in out Printer;
      Obj  : in out Exprs.Unary'Class)
   is
   begin
      Append (AST_Output, "(");
      Append (AST_Output, Obj.Operator.Kind'Image);
      Append (AST_Output, " ");
      Obj.Right.Acceptor (Self'Access);
      Append (AST_Output, ")");
   end Visit_Unary;

   overriding procedure Visit_Variable
     (Self : in out Printer;
      Obj  : in out Exprs.Variable'Class)
   is
   begin
      Append (AST_Output, Tokens.Lexeme_Str.To_String (Obj.Name.Lexeme));
   end Visit_Variable;

   overriding procedure Visit_Block
     (Self : in out Printer;
      Obj  : in out Stmts.Block'Class)
   is
   begin
      Append (AST_Output, "[block");
      Append (AST_Output, Ada.Characters.Latin_9.LF);
      for S of Obj.Statements loop
         Append (AST_Output, "  ");
         S.Acceptor (Self'Access);
         Append (AST_Output, Ada.Characters.Latin_9.LF);
      end loop;
      Append (AST_Output, "]");
   end Visit_Block;

   overriding procedure Visit_Class
     (Self : in out Printer;
      Obj  : in out Stmts.Class'Class)
   is
   begin
      Append (AST_Output, "[class ");
      Append (AST_Output, Tokens.Lexeme_Str.To_String (Obj.Name.Lexeme));
      Append (AST_Output, Ada.Characters.Latin_9.LF);
      for S of Obj.Methods loop
         Append (AST_Output, "  ");
         S.Acceptor (Self'Access);
         Append (AST_Output, Ada.Characters.Latin_9.LF);
      end loop;
      Append (AST_Output, "]");
   end Visit_Class;

   overriding procedure Visit_Expression
     (Self : in out Printer;
      Obj  : in out Stmts.Expression'Class)
   is
   begin
      Append (AST_Output, "[");
      Obj.Ex.Acceptor (Self'Access);
      Append (AST_Output, "]");
   end Visit_Expression;

   overriding procedure Visit_Function_S
     (Self : in out Printer;
      Obj  : in out Stmts.Function_S'Class)
   is
   begin
      Append (AST_Output, "[function ");
      Append (AST_Output, Tokens.Lexeme_Str.To_String (Obj.Name.Lexeme));
      Append (AST_Output, " [param");
      for T of Obj.Parameters loop
         Append (AST_Output, " ");
         Append (AST_Output, Tokens.Lexeme_Str.To_String (T.Lexeme));
      end loop;
      Append (AST_Output, "]");

      Append (AST_Output, Ada.Characters.Latin_9.LF);
      for S of Obj.Statements loop
         S.Acceptor (Self'Access);
         Append (AST_Output, Ada.Characters.Latin_9.LF);
      end loop;
      Append (AST_Output, "]");
   end Visit_Function_S;

   overriding procedure Visit_If_S
     (Self : in out Printer;
      Obj  : in out Stmts.If_S'Class)
   is
   begin
      Append (AST_Output, "[if ");
      -- Obj.Right.Acceptor (Self'Access);
      Append (AST_Output, "]");
   end Visit_If_S;

   overriding procedure Visit_Print
     (Self : in out Printer;
      Obj  : in out Stmts.Print'Class)
   is
   begin
      Append (AST_Output, "[print ");
      Obj.Ex.Acceptor (Self'Access);
      Append (AST_Output, "]");
   end Visit_Print;

   overriding procedure Visit_Var
     (Self : in out Printer;
      Obj  : in out Stmts.Var'Class)
   is
   begin
      Append (AST_Output, "[var ");
      Append (AST_Output, Tokens.Lexeme_Str.To_String (Obj.Name.Lexeme));
      Append (AST_Output, " ");
      declare
         use Exprs;
      begin
         if Obj.Initializer /= null then
            Obj.Initializer.Acceptor (Self'Access);
         else
            Append (AST_Output, "null");
         end if;
      end;
      Append (AST_Output, "]");
   end Visit_Var;

   overriding procedure Visit_Return_S
     (Self : in out Printer;
      Obj  : in out Stmts.Return_S'Class)
   is
      use type Exprs.Expr_Acc;
   begin
      Append (AST_Output, "[return ");
      if Obj.Value /= null then
         Obj.Value.Acceptor (Self'Access);
      end if;
      Append (AST_Output, "]");
   end Visit_Return_S;

   overriding procedure Visit_While_S
     (Self : in out Printer;
      Obj  : in out Stmts.While_S'Class)
   is
   begin
      Append (AST_Output, "[while ");
      Obj.Condition.Acceptor (Self'Access);
      Append (AST_Output, " ");
      Obj.Loop_Body.Acceptor (Self'Access);
      Append (AST_Output, "]");
   end Visit_While_S;
end AST_Printer;
